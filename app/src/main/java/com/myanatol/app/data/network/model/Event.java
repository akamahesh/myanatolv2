package com.myanatol.app.data.network.model;

import com.myanatol.app.managers.ModelManager.ModelManager;

import org.json.JSONObject;

public class Event extends BaseModel {

    private String id;

    private String createdAt;

    private String updatedAt;

    private String title;

    private String commonEventId;

    private String user;

    private Double v;

    private DateTime endDatetime;

    private DateTime startDatetime;

    public Event(JSONObject jsonResponse) {
        this.id = getValue(jsonResponse, kId, String.class);
        this.title = getValue(jsonResponse, kTitle, String.class);
        this.commonEventId = getValue(jsonResponse, kCommonEventId, String.class);
        this.user = getValue(jsonResponse, kUser, String.class);
        this.v = getValue(jsonResponse, kV, Double.class);
        this.startDatetime = new DateTime(getValue(jsonResponse, kStartDateTime, JSONObject.class));
        this.endDatetime = new DateTime(getValue(jsonResponse, kEndDateTime, JSONObject.class));

    }

    public Event(String id, String title, String commonEventId,  DateTime startDatetime, DateTime endDatetime) {
        this.id = id;
        this.title = title;
        this.commonEventId = commonEventId;
        this.user = ModelManager.modelManager().getCurrentUser().getId();
        this.v = 0.0;
        this.endDatetime = endDatetime;
        this.startDatetime = startDatetime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCommonEventId() {
        return commonEventId;
    }

    public void setCommonEventId(String commonEventId) {
        this.commonEventId = commonEventId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Double getV() {
        return v;
    }

    public void setV(Double v) {
        this.v = v;
    }

    public DateTime getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(DateTime endDatetime) {
        this.endDatetime = endDatetime;
    }

    public DateTime getStartDatetime() {
        return startDatetime;
    }

    public void setStartDatetime(DateTime startDatetime) {
        this.startDatetime = startDatetime;
    }

}
