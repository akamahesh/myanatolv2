package com.myanatol.app.data.network.model;

import java.util.Calendar;

public class EventCalendar extends BaseModel{
    private String name;
    private String description;
    private String location;
    private Calendar startDate;
    private Calendar endDate;

    public EventCalendar(String name, String description, String location, String startDate, String endDate) {
        this.name = name;
        this.description = description;
        this.location = location;
        this.startDate = Calendar.getInstance();
        this.startDate.setTimeInMillis(Long.parseLong(startDate));
        this.endDate = Calendar.getInstance();
        this.endDate.setTimeInMillis(Long.parseLong(endDate));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "EventCalendar{" +
                "name='" + name +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        EventCalendar eventCalendar = (EventCalendar) obj;
        return this.getName().equals(eventCalendar.getName());
    }
}
