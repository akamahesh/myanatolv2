package com.myanatol.app.data;

import com.myanatol.app.data.network.model.Metro;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton class
 * Serves as DataRepository, provides data to viewModel from db and network calls
 */
public class DataManager {
    private static DataManager _DataManager;

    /**
     * Singleton class
     * @return
     */
    public synchronized static DataManager getDataManager() {
        if (_DataManager == null) {
            _DataManager = new DataManager();
        }
        return _DataManager;
    }

    public List<Metro> getMetroList() {
        List<Metro> metros = new ArrayList<>();
        metros.add(new Metro("Adana, Turkey", "mametro_Adana_Turkey.webp"));
        metros.add(new Metro("Algiers, Algeria", "mametro_Algiers_Algeria.webp"));
        metros.add(new Metro("Almaty, Kazakhstan", "mametro_Almaty_Kazakhstan.webp"));
        metros.add(new Metro("Amsterdam, Netherlands", "mametro_Amsterdam_Netherlands.webp"));
        metros.add(new Metro("Athens, Greece", "mametro_Athens_Greece.webp"));
        metros.add(new Metro("Atlanta, USA", "mametro_Atlanta_USA.webp"));
        metros.add(new Metro("Baku, Azerbaijan", "mametro_Baku_Azerbaijan.webp"));
        metros.add(new Metro("Baltimore, USA", "mametro_Baltimore_USA.webp"));
        metros.add(new Metro("Bangalore, India", "mametro_Bangalore_India.webp"));
        metros.add(new Metro("Bangkok, Thailand", "mametro_Bangkok_Thailand.webp"));
        metros.add(new Metro("Barcelona, Spain", "mametro_Barcelona_Spain.png"));
        metros.add(new Metro("Beijing, China", "mametro_Beijing_China.webp"));
        metros.add(new Metro("Belo Horizonte, Brazil", "mametro_Belo_Horizonte_Brazil.webp"));
        metros.add(new Metro("Berlin, Germany", "mametro_Berlin_Germany.webp"));
        metros.add(new Metro("Bilbao, Spain", "mametro_Bilbao_Spain.png"));
        metros.add(new Metro("Bordeaux, France", "mametro_Bordeaux_France.webp"));
        metros.add(new Metro("Boston, USA", "mametro_Boston_USA.png"));
        metros.add(new Metro("Brasilia, Brazil", "mametro_Brasilia_Brazil.webp"));
        metros.add(new Metro("Brescia, Italy", "mametro_Brescia_Italy.png"));
        metros.add(new Metro("Brussels, Belgium", "mametro_Brussels_Belgium.png"));
        metros.add(new Metro("Bucharest, Romania", "mametro_Bucharest_Romania.webp"));
        metros.add(new Metro("Budapest, Hungary", "mametro_Budapest_Hungary.webp"));
        metros.add(new Metro("Buenos Aires, Argentina", "mametro_Buenos_Aires_Argentina.webp"));
        metros.add(new Metro("Bursa, Turkey", "mametro_Bursa_Turkey.png"));
        metros.add(new Metro("Busan, South Korea", "mametro_Busan_South_Korea.webp"));
        metros.add(new Metro("Cairo, Egypt", "mametro_Cairo_Egypt.webp"));
        metros.add(new Metro("Caracas, Venezuela", "mametro_Caracas_Venezuela.png"));
        metros.add(new Metro("Catania, Italy", "mametro_Catania_Italy.webp"));
        metros.add(new Metro("Changchun, China", "mametro_Changchun_China.png"));
        metros.add(new Metro("Changdu, China", "mametro_Changdu_China.png"));
        metros.add(new Metro("Changsha, China", "mametro_Changsha_China.webp"));
        metros.add(new Metro("Chennai, India", "mametro_Chennai_India.png"));
        metros.add(new Metro("Chicago, USA", "mametro_Chicago_USA.png"));
        metros.add(new Metro("Chongqing, China", "mametro_Chongqing_China.webp"));
        metros.add(new Metro("Cleveland, USA", "mametro_Cleveland_USA.png"));
        metros.add(new Metro("Copenhagen, Denmark", "mametro_Copenhagen_Denmark.png"));
        metros.add(new Metro("Daegu, South Korea", "mametro_Daegu_South_Korea.webp"));
        metros.add(new Metro("Daejeon, South Korea", "mametro_Daejeon_South_Korea.png"));
        metros.add(new Metro("Dalian, China", "mametro_Dalian_China.webp"));
        metros.add(new Metro("Delhi, India", "mametro_Delhi_India.webp"));
        metros.add(new Metro("Dnipro, Ukraine", "mametro_Dnipro_Ukraine.png"));
        metros.add(new Metro("Dongguan, China", "mametro_Dongguan_China.webp"));
        metros.add(new Metro("Dubai, United Arab Emirates", "mametro_Dubai_United_Arab_Emirates.webp"));
        metros.add(new Metro("Foshan, China", "mametro_Foshan_China.webp"));
        metros.add(new Metro("Fukuoka, Japan", "mametro_Fukuoka_Japan.png"));
        metros.add(new Metro("Fuzhou, Chin", "mametro_Fuzhou_China.webp"));
        metros.add(new Metro("Genoa, Italy", "mametro_Genoa_Italy.png"));
        metros.add(new Metro("Glasgow, UK", "mametro_Glasgow_UK.png"));
        metros.add(new Metro("Guangzhou, China", "mametro_Guangzhou_China.webp"));
        metros.add(new Metro("Gurgaon, India", "mametro_Gurgaon_India.webp"));
        metros.add(new Metro("Gwangju, South Korea", "mametro_Gwangju_South_Korea.webp"));
        metros.add(new Metro("Hamburg, Germany", "mametro_Hamburg_Germany.png"));
        metros.add(new Metro("Hangzhou, China", "mametro_Hangzhou_China.webp"));
        metros.add(new Metro("Harbin, China", "mametro_Harbin_China.webp"));
        metros.add(new Metro("Hefei, China", "mametro_Hefei_China.webp"));
        metros.add(new Metro("Helsinki, Finland", "mametro_Helsinki_Finland.webp"));
        metros.add(new Metro("Hiroshima, Japan", "mametro_Hiroshima_Japan.png"));
        metros.add(new Metro("Hong Kong, China", "mametro_Hong_Kong_China.webp"));
        metros.add(new Metro("Incheon, South Korea", "mametro_Incheon_South_Korea.png"));
        metros.add(new Metro("Isfahan, Iran", "mametro_Isfahan_Iran.png"));
        metros.add(new Metro("Istanbul, Turkey", "mametro_Istanbul_Turkey.png"));
        metros.add(new Metro("Izmir, Turkey", "mametro_Izmir_Turkey.webp"));
        metros.add(new Metro("Jaipur, India", "mametro_Jaipur_India.webp"));
        metros.add(new Metro("Kaohsiung, Taiwan", "mametro_Kaohsiung_Taiwan.webp"));
        metros.add(new Metro("Kazan, Russia", "mametro_Kazan_Russia.webp"));
        metros.add(new Metro("Kharkiv, Ukraine", "mametro_Kharkiv_Ukraine.png"));
        metros.add(new Metro("Kiev, Ukraine", "mametro_Kiev_Ukraine.png"));
        metros.add(new Metro("Kobe, Japan", "mametro_Kobe_Japan.png"));
        metros.add(new Metro("Kochi, India", "mametro_Kochi_India.webp"));
        metros.add(new Metro("Kolkata, India", "mametro_Kolkata_India.webp"));
        metros.add(new Metro("Kuala Lumpur, Malaysia", "mametro_Kuala_Lumpur_Malaysia.webp"));
        metros.add(new Metro("Kunming, China", "mametro_Kunming_China.png"));
        metros.add(new Metro("Kyoto, Japan", "mametro_Kyoto_Japan.png"));
        metros.add(new Metro("Lausanne, Switzerland", "mametro_Lausanne_Switzerland.png"));
        metros.add(new Metro("Lille, France", "mametro_Lille_France.png"));
        metros.add(new Metro("Lima, Peru", "mametro_Lima_Peru.webp"));
        metros.add(new Metro("Lisbon, Portugal", "mametro_Lisbon_Portugal.webp"));
        metros.add(new Metro("London, UK", "mametro_London_UK.png"));
        metros.add(new Metro("Los Angeles, USA", "mametro_Los_Angeles_USA.webp"));
        metros.add(new Metro("Lucknow, India", "mametro_Lucknow_India.png"));
        metros.add(new Metro("Lyon, France", "mametro_Lyon_France.webp"));
        metros.add(new Metro("Madrid, Spain", "mametro_Madrid_Spain.png"));
        metros.add(new Metro("Manila, Philippines", "mametro_Manila_Philippines.webp"));
        metros.add(new Metro("Marseille, France", "mametro_Marseille_France.png"));
        metros.add(new Metro("Mashhad, Iran", "mametro_Mashhad_Iran.webp"));
        metros.add(new Metro("Mecca, Saudi Arabia", "mametro_Mecca_Saudi_Arabia.png"));
        metros.add(new Metro("Medellin, Colombia", "mametro_Medellin_Colombia.webp"));
        metros.add(new Metro("Mexico City, Mexico", "mametro_Mexico_City_Mexico.webp"));
        metros.add(new Metro("Miamy, USA", "mametro_Miamy_USA.png"));
        metros.add(new Metro("Milan, Italy", "mametro_Milan_Italy.png"));
        metros.add(new Metro("Minsk, Belarus", "mametro_Minsk_Belarus.webp"));
        metros.add(new Metro("Monterrey, Mexico", "mametro_Monterrey_Mexico.png"));
        metros.add(new Metro("Montreal, Canada", "mametro_Montreal_Canada.webp"));
        metros.add(new Metro("Moscow, Russia", "mametro_Moscow_Russia.webp"));
        metros.add(new Metro("Mumbai, India", "mametro_Mumbai_India.webp"));
        metros.add(new Metro("Munich, Germany", "mametro_Munich_Germany.webp"));
        metros.add(new Metro("Nagoya, Japan", "mametro_Nagoya_Japan.png"));
        metros.add(new Metro("Nanchang, China", "mametro_Nanchang_China.png"));
        metros.add(new Metro("Nanjing, China", "mametro_Nanjing_China.webp"));
        metros.add(new Metro("Nanning, China", "mametro_Nanning_China.png"));
        metros.add(new Metro("Naples, Italy", "mametro_Naples_Italy.png"));
        metros.add(new Metro("New York City, USA", "mametro_New_York_City_USA.webp"));
        metros.add(new Metro("Newcastle, UK", "mametro_Newcastle_UK.png"));
        metros.add(new Metro("Ningbo, China", "mametro_Ningbo_China.png"));
        metros.add(new Metro("Nizhny Novgorod, Russia", "mametro_Nizhny_Novgorod_Russia.webp"));
        metros.add(new Metro("Novosibirsk, Russia", "mametro_Novosibirsk_Russia.png"));
        metros.add(new Metro("Nuremberg, Germany", "mametro_Nuremberg_Germany.png"));
        metros.add(new Metro("Osaka, Japan", "mametro_Osaka_Japan.png"));
        metros.add(new Metro("Oslo, Norway", "mametro_Oslo_Norway.png"));
        metros.add(new Metro("Panama City, Panama", "mametro_Panama_City_Panama.webp"));
        metros.add(new Metro("Paris, France", "mametro_Paris_France.webp"));
        metros.add(new Metro("Philadelphia, USA", "mametro_Philadelphia_USA.png"));
        metros.add(new Metro("Porto Alegre, Brazil", "mametro_Porto_Alegre_Brazil.png"));
        metros.add(new Metro("Prague, Czech Republic", "mametro_Prague_Czech_Republic.png"));
        metros.add(new Metro("Pyongyang, North Korea", "mametro_Pyongyang_North_Korea.webp"));
        metros.add(new Metro("Recife, Brazil", "mametro_Recife_Brazil.png"));
        metros.add(new Metro("Rennes, France", "mametro_Rennes_France.webp"));
        metros.add(new Metro("Rio de Janeiro, Brazil", "mametro_Rio_de_Janeiro_Brazil.png"));
        metros.add(new Metro("Rome, Italy", "mametro_Rome_Italy.png"));
        metros.add(new Metro("Rotterdam, Netherlands", "mametro_Rotterdam_Netherlands.png"));
        metros.add(new Metro("Saint Petersburg, Russia", "mametro_Saint_Petersburg_Russia.webp"));
        metros.add(new Metro("Salvador, Brazil", "mametro_Salvador_Brazil.dms"));
        metros.add(new Metro("Samara, Russia", "mametro_Samara_Russia.webp"));
        metros.add(new Metro("San Francisco, USA", "mametro_San_Francisco_USA.webp"));
        metros.add(new Metro("Santiago, Chile", "mametro_Santiago_Chile.webp"));
        metros.add(new Metro("Santo Domingo, Dominican Republic", "mametro_Santo_Domingo_Dominican_Republic.png"));
        metros.add(new Metro("Sapporo, Japan", "mametro_Sapporo_Japan.png"));
        metros.add(new Metro("Sendai, Japan", "mametro_Sendai_Japan.png"));
        metros.add(new Metro("Seoul, South Korea", "mametro_Seoul_South_Korea.png"));
        metros.add(new Metro("Shanghai, China", "mametro_Shanghai_China.png"));
        metros.add(new Metro("Shenzhen, China", "mametro_Shenzhen_China.webp"));
        metros.add(new Metro("Singapore, Singapore", "mametro_Singapore_Singapore.webp"));
        metros.add(new Metro("Sofia, Bulgaria", "mametro_Sofia_Bulgaria.png"));
        metros.add(new Metro("Stockholm, Sweden", "mametro_Stockholm_Sweden.webp"));
        metros.add(new Metro("Suzhou, China", "mametro_Suzhou_China.webp"));
        metros.add(new Metro("Tabriz, Iran", "mametro_Tabriz_Iran.png"));
        metros.add(new Metro("Taipei, Taiwan", "mametro_Taipei_Taiwan.png"));
        metros.add(new Metro("Taoyuan, Taiwan", "mametro_Taoyuan_Taiwan.png"));
        metros.add(new Metro("Tashkent, Uzbekistan", "mametro_Tashkent_Uzbekistan.webp"));
        metros.add(new Metro("Tbilisi, Georgia", "mametro_Tbilisi_Georgia.png"));
        metros.add(new Metro("Tehran, Iran", "mametro_Tehran_Iran.webp"));
        metros.add(new Metro("Tianjin, China", "mametro_Tianjin_China.png"));
        metros.add(new Metro("Tokyo, Japan", "mametro_Tokyo_Japan.png"));
        metros.add(new Metro("Toronto, Canada", "mametro_Toronto_Canada.webp"));
        metros.add(new Metro("Toulouse, France", "mametro_Toulouse_France.png"));
        metros.add(new Metro("Turin, Italy", "mametro_Turin_Italy.webp"));
        metros.add(new Metro("Vancouver, Canada", "mametro_Vancouver_Canada.webp"));
        metros.add(new Metro("Vienna, Austria", "mametro_Vienna_Austria.webp"));
        metros.add(new Metro("Warsaw, Poland", "mametro_Warsaw_Poland.webp"));
        metros.add(new Metro("Washington DC, USA", "mametro_WashingtonDC_USA.png"));
        metros.add(new Metro("Wuhan, China", "mametro_Wuhan_China.png"));
        metros.add(new Metro("Wuxi, China", "mametro_Wuxi_China.png"));
        metros.add(new Metro("Xian, China", "mametro_Xian_China.webp"));
        metros.add(new Metro("Yekaterinburg, Russia", "mametro_Yekaterinburg_Russia.png"));
        metros.add(new Metro("Yerevan, Armenia", "mametro_Yerevan_Armenia.webp"));
        metros.add(new Metro("Yokohama, Japan", "mametro_Yokohama_Japan.webp"));
        metros.add(new Metro("Zhengzhou, China", "mametro_Zhengzhou_China.webp"));


        return metros;
    }
}
