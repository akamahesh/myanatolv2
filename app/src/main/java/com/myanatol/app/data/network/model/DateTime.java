package com.myanatol.app.data.network.model;

import com.myanatol.app.helper.Utils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTime extends BaseModel {

    private String value;
    private String text;
    private String timeZone;
    private SimpleDateFormat formatter;
    private Calendar calender;

    DateTime(JSONObject jsonResponse) {
        double dvalue = getValue(jsonResponse, kValue, Double.class);
        long lvalue= (Double.valueOf(dvalue)).longValue();
        this.value = String.valueOf(lvalue);
        this.text = getValue(jsonResponse, kText, String.class);

        this.timeZone = getValue(jsonResponse, kTimezone, String.class);
        formatter = new SimpleDateFormat("HH:MM a");
        this.calender = Calendar.getInstance();
        this.calender.setTimeInMillis(Long.parseLong(value));
    }

    public DateTime(String timeStamp, int type) {
        double dvalue = Double.valueOf(timeStamp);
        this.value = String.valueOf(dvalue).replace(".","");
        formatter = new SimpleDateFormat("HH:MM a");
        this.text = formatter.format(new Date(Long.parseLong(timeStamp)));
        this.timeZone = Utils.getCurrentTimeZone().getID();

        this.calender = Calendar.getInstance();
        this.calender.setTimeInMillis(Long.parseLong(timeStamp));
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public Calendar getCalender() {
        return calender;
    }

    public void setCalender(Calendar calender) {
        this.calender = calender;
    }
}
