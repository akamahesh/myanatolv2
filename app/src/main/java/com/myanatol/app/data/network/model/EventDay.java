package com.myanatol.app.data.network.model;

public class EventDay extends BaseModel {
    private String day;
    private int date;
    private boolean isSelected = false;

    public EventDay(String day, int date) {
        this.day = day;
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
