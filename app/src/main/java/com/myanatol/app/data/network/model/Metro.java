package com.myanatol.app.data.network.model;

import java.io.Serializable;

public class Metro extends BaseModel {
    private String name;
    private String path;
    private boolean isSelected = false;

    public Metro(String name, String path) {
        this.name = name;
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
