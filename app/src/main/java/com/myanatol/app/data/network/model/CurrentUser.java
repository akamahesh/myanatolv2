package com.myanatol.app.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.List;

public class CurrentUser extends BaseModel {

    private String id;
    private String displayName;
    private String phone;
    private String email;
    private String password;
    private String homePlaceId;
    private String businessPlaceId;
    private String favouritePlaceId;
    private Boolean isActivated;
    private Boolean isSuperuser;
    private Name name;
    private List<Object> photos = null;
    private List<Object> roles = null;
    private Integer v;
    private String provider;
    private String updatedAt;
    private String createdAt;
    private String commonEventId = "";

    public CurrentUser(JSONObject jsonResponse) {
        this.id                 = getValue(jsonResponse,kId,String.class);
        this.displayName        = getValue(jsonResponse,kDisplayName,String.class);
        this.phone              = getValue(jsonResponse,kPhone,String.class);
        this.email              = getValue(jsonResponse,kEmail,String.class);
        this.password           = getValue(jsonResponse,kPassword,String.class);
        this.homePlaceId        = getValue(jsonResponse,kHomePlaceId,String.class);
        this.businessPlaceId    = getValue(jsonResponse,kBusinessPlaceId,String.class);
        this.favouritePlaceId   = getValue(jsonResponse,kFavoritePlaceId,String.class);
        this.isActivated        = getValue(jsonResponse,kIsActivated,Boolean.class);
        this.isSuperuser        = getValue(jsonResponse,kIsSuperUser,Boolean.class);
        this.provider           = getValue(jsonResponse,kProvider,String.class);
        this.updatedAt          = getValue(jsonResponse,kUpdateAt,String.class);
        this.createdAt          = getValue(jsonResponse,kCreatedAt,String.class);
    }

     /*"familyName": "McFly",
            "givenName": "Martin",
            "middleName": "Seamus"*/
    public class Name {
        String familyName;
        String givenName;
        String middleName;

         public Name() {
         }

         Name(JSONObject jsonResponse){
            this.familyName                = getValue(jsonResponse,kFamilyName,String.class);
            this.givenName                 = getValue(jsonResponse,kGivenName,String.class);
            this.middleName                = getValue(jsonResponse,kMiddleName,String.class);
        }

         public String getFamilyName() {
             return (familyName!=null)?familyName:"";
         }

         public void setFamilyName(String familyName) {
             this.familyName = familyName;
         }

         public String getGivenName() {
             return (givenName!=null)?givenName:"";
         }

         public void setGivenName(String givenName) {
             this.givenName = givenName;
         }

         public String getMiddleName() {
             return (middleName!=null)?middleName:"";
         }

         public void setMiddleName(String middleName) {
             this.middleName = middleName;
         }
     }

    public String getCommonEventId() {
        return commonEventId;
    }

    public void setCommonEventId(String commonEventId) {
        this.commonEventId = commonEventId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHomePlaceId() {
        return homePlaceId;
    }

    public void setHomePlaceId(String homePlaceId) {
        this.homePlaceId = homePlaceId;
    }

    public String getBusinessPlaceId() {
        return businessPlaceId;
    }

    public void setBusinessPlaceId(String businessPlaceId) {
        this.businessPlaceId = businessPlaceId;
    }

    public String getFavouritePlaceId() {
        return favouritePlaceId;
    }

    public void setFavouritePlaceId(String favouritePlaceId) {
        this.favouritePlaceId = favouritePlaceId;
    }

    public Boolean getActivated() {
        return isActivated;
    }

    public void setActivated(Boolean activated) {
        isActivated = activated;
    }

    public Boolean getSuperuser() {
        return isSuperuser;
    }

    public void setSuperuser(Boolean superuser) {
        isSuperuser = superuser;
    }

    public Name getName() {
        return (name==null)?new Name():name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public List<Object> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Object> photos) {
        this.photos = photos;
    }

    public List<Object> getRoles() {
        return roles;
    }

    public void setRoles(List<Object> roles) {
        this.roles = roles;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
