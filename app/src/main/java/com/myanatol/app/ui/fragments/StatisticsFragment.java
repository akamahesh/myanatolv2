package com.myanatol.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import com.myanatol.app.R;
import com.myanatol.app.helper.FragmentUtil;
import com.myanatol.app.ui.activity.HomeActivity;
import com.myanatol.app.ui.adapter.MetroAdapter;

import java.util.Objects;

/**
 * ProfuileFragment to for user profile, attached to {@link HomeActivity}
 */
public class StatisticsFragment extends Fragment {

    private TextView tvThisMonth;
    private TextView tvHistory;
    private TextView tvPrevMonth;


    public StatisticsFragment() {
        // Required empty public constructor
    }


    public static StatisticsFragment newInstance() {
        StatisticsFragment fragment = new StatisticsFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistics, container, false);
        initViews(view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvThisMonth.performClick();
    }




    private void initViews(View view) {
        tvHistory = view.findViewById(R.id.tvHistory);
        tvThisMonth = view.findViewById(R.id.tvThisMonth);
        tvPrevMonth = view.findViewById(R.id.tvPreviousMonth);
        tvHistory.setOnClickListener(v-> onHistory());
        tvThisMonth.setOnClickListener(v-> onThisMonth());
        tvPrevMonth.setOnClickListener(v-> onPreviousMonth());

    }

    private void onThisMonth() {
        initialTabState();
        tvThisMonth.setBackground(getResources().getDrawable(R.drawable.canvas_tab_selected_bg,null));
        tvThisMonth.setTextColor(getResources().getColor(R.color.colorWindowWhite));
        loadThisMonth();
    }



    private void onPreviousMonth() {
        initialTabState();
        tvPrevMonth.setBackground(getResources().getDrawable(R.drawable.canvas_tab_selected_bg,null));
        tvPrevMonth.setTextColor(getResources().getColor(R.color.colorWindowWhite));
        loadPrevousMonth();
    }

    private void onHistory() {
        initialTabState();
        tvHistory.setBackground(getResources().getDrawable(R.drawable.canvas_tab_selected_bg,null));
        tvHistory.setTextColor(getResources().getColor(R.color.colorWindowWhite));
        loadHistory();
    }

    private void loadThisMonth() {
        Fragment monthRecordFragment = getChildFragmentManager().findFragmentByTag(MonthRecordFragment.class.getName());
        if(monthRecordFragment==null)
            FragmentUtil.changeFragment(getChildFragmentManager(),MonthRecordFragment.newInstance(),false,true);
        else
            FragmentUtil.changeFragment(getChildFragmentManager(),monthRecordFragment,false,true);
    }

    private void loadHistory() {
        Fragment monthRecordFragment = getChildFragmentManager().findFragmentByTag(MonthRecordFragment.class.getName());
        if(monthRecordFragment==null)
            FragmentUtil.changeFragment(getChildFragmentManager(),MonthRecordFragment.newInstance(),false,true);
        else
            FragmentUtil.changeFragment(getChildFragmentManager(),monthRecordFragment,false,true);

    }

    private void loadPrevousMonth() {
        Fragment monthRecordFragment = getChildFragmentManager().findFragmentByTag(MonthRecordFragment.class.getName());
        if(monthRecordFragment==null)
            FragmentUtil.changeFragment(getChildFragmentManager(),MonthRecordFragment.newInstance(),false,true);
        else
            FragmentUtil.changeFragment(getChildFragmentManager(),monthRecordFragment,false,true);

    }



    private void initialTabState() {
        tvHistory.setBackground(getResources().getDrawable(R.drawable.canvas_tab_unselected_bg,null));
        tvHistory.setTextColor(getResources().getColor(R.color.textColorDarkSecondary));
        tvThisMonth.setBackground(getResources().getDrawable(R.drawable.canvas_tab_unselected_bg,null));
        tvThisMonth.setTextColor(getResources().getColor(R.color.textColorDarkSecondary));
        tvPrevMonth.setBackground(getResources().getDrawable(R.drawable.canvas_tab_unselected_bg,null));
        tvPrevMonth.setTextColor(getResources().getColor(R.color.textColorDarkSecondary));
    }

    @Override
    public void onStart() {
        super.onStart();
        //updates toobar title
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.title_statistics));
    }



}
