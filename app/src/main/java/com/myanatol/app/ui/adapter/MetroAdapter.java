package com.myanatol.app.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.myanatol.app.R;
import com.myanatol.app.data.network.model.Metro;

import java.util.ArrayList;
import java.util.List;

public class MetroAdapter extends RecyclerView.Adapter<MetroAdapter.ViewHolder> implements Filterable {

    private Context context;
    private List<Metro> metroList;
    private List<Metro> metroListFiltered;
    private MetroInteraction mListener;

    public MetroAdapter(Context context, List<Metro> metroList, MetroInteraction metroInteraction) {
        this.context = context;
        this.metroList = metroList;
        this.metroListFiltered = metroList;
        this.mListener = metroInteraction;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_metro_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Metro metro = metroListFiltered.get(position);
        holder.bindContent(metro);
        holder.tvMetro.setText(metro.getName());

    }

    @Override
    public int getItemCount() {
        return metroListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    metroListFiltered = metroList;
                } else {
                    List<Metro> filteredList = new ArrayList<>();
                    for (Metro row : metroList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    metroListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = metroListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                metroListFiltered = (ArrayList<Metro>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface MetroInteraction {
        void onMetroSelection(Metro metro);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivSelection;
        private TextView tvMetro;
        private Metro metro;

        ViewHolder(View itemView) {
            super(itemView);
            tvMetro = itemView.findViewById(R.id.tvMetro);
            itemView.setOnClickListener(v -> {
                if (mListener != null)
                    mListener.onMetroSelection(metro);
            });
        }

        void bindContent(Metro metro) {
            this.metro = metro;
        }
    }
}
