package com.myanatol.app.ui.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.facebook.login.widget.LoginButton;
import com.myanatol.app.R;
import com.myanatol.app.helper.Utils;
import com.myanatol.app.helper.Validations;
import com.myanatol.app.ui.activity.LoginActivity;

import java.util.Collections;
import java.util.Objects;

import static com.myanatol.app.constants.Constants.kEmail;

/**
 * LoginFragment for login screen of login module attached to {@link com.myanatol.app.ui.activity.LoginActivity}
 */
public class NewLoginFragment extends BaseFragment {

    private static final String TAG = NewLoginFragment.class.getSimpleName();
    private OnLoginInteractionListener mListener;
    private EditText edtEmail;


    public NewLoginFragment() {
        // Required empty public constructor
    }


    public static NewLoginFragment newInstance() {
        NewLoginFragment fragment = new NewLoginFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLoginInteractionListener) {
            mListener = (OnLoginInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLoginInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_login, container, false);
        Utils.hideKeyboard(getContext());
        initViews(view);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((LoginActivity) Objects.requireNonNull(getActivity())).requestCredential();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void initViews(View view) {
        edtEmail = view.findViewById(R.id.edtEmail);
        LoginButton loginButton = view.findViewById(R.id.loginButton);
        loginButton.setReadPermissions(Collections.singletonList(kEmail));
        loginButton.setFragment(this);

        view.findViewById(R.id.btnLogin).setOnClickListener(v -> onLogin());
        view.findViewById(R.id.btnFacebook).setOnClickListener(v -> onFacebook());
    }

    private void onFacebook() {
        if (mListener != null) {
            mListener.onFacebook();
        }
    }


    /**
     * Email Login
     */
    private void onLogin() {
        if (!validate()) {
            return;
        }
        String email = Utils.getProperText(edtEmail);
        if (mListener != null) {
            mListener.onLogin(email);
        }
    }

    private boolean validate() {
        boolean isValid = true;
        String email = Utils.getProperText(edtEmail);
        if (!Validations.isValidEmail(email)) {
            edtEmail.setError(getString(R.string.validation_email));
            edtEmail.requestFocus();
            isValid = false;
        }

        return isValid;
    }

    public void setEmail(String email) {
        edtEmail.setText(email);
    }


    public interface OnLoginInteractionListener {
        void onLogin(String email);
        void onFacebook();
    }
}

