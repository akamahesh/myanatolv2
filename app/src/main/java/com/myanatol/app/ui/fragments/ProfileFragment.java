package com.myanatol.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.TextView;

import com.myanatol.app.R;
import com.myanatol.app.constants.Constants;
import com.myanatol.app.data.network.model.CurrentUser;
import com.myanatol.app.managers.ModelManager.ModelManager;
import com.myanatol.app.ui.activity.HomeActivity;
import com.myanatol.app.ui.activity.LoginActivity;

import java.util.Objects;

/**
 * ProfileFragment to for user profile, attached to {@link HomeActivity}
 */
public class ProfileFragment extends Fragment {

    private TextView tvDeleteProfile;
    private CurrentUser currentUser;
    private EditText edtFirstName;
    private EditText edtLastName;
    private EditText edtMobileNumber;
    private EditText edtBirthDate;
    private EditText edtEmail;
    private EditText edtPassword;


    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        currentUser = ModelManager.modelManager().getCurrentUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateUI(currentUser);
    }

    private void updateUI(CurrentUser currentUser) {
        if (currentUser == null)
            return;
        edtFirstName.setText(currentUser.getName().getGivenName());
        edtLastName.setText(currentUser.getName().getFamilyName());
        edtMobileNumber.setText(currentUser.getPhone());
        edtEmail.setText(currentUser.getEmail());
    }

    private void initViews(View view) {
        tvDeleteProfile = view.findViewById(R.id.tvDeleteProfile);
        tvDeleteProfile.setOnClickListener(v -> deleteProfile());
        view.findViewById(R.id.tvLogout).setOnClickListener(v -> deleteProfile());
        edtFirstName = view.findViewById(R.id.edtFirstName);
        edtLastName = view.findViewById(R.id.edtLastName);
        edtMobileNumber = view.findViewById(R.id.edtMobileNumber);
        edtBirthDate = view.findViewById(R.id.edtBirthDate);
        edtEmail = view.findViewById(R.id.edtEmail);
        edtPassword = view.findViewById(R.id.edtPassword);
    }

    private void deleteProfile() {
        ModelManager.modelManager().setCurrentUser(null);
        startActivity(LoginActivity.getIntent(getContext(), Constants.LOGIN_FROM.login, ""));
        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        //updates toobar title
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.title_profile));
    }

}
