package com.myanatol.app.ui.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.myanatol.app.R;
import com.myanatol.app.data.network.model.Event;
import com.myanatol.app.helper.Toaster;
import com.myanatol.app.helper.Utils;
import com.myanatol.app.managers.ModelManager.ModelManager;
import com.myanatol.app.ui.activity.HomeActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.myanatol.app.constants.Constants.PLACE_AUTOCOMPLETE_REQUEST_CODE;
import static com.myanatol.app.constants.Constants.kCommonEventId;
import static com.myanatol.app.constants.Constants.kData;
import static com.myanatol.app.constants.Constants.kEndDateTime;
import static com.myanatol.app.constants.Constants.kStartDateTime;
import static com.myanatol.app.constants.Constants.kText;
import static com.myanatol.app.constants.Constants.kTimezone;
import static com.myanatol.app.constants.Constants.kTitle;
import static com.myanatol.app.constants.Constants.kType;
import static com.myanatol.app.constants.Constants.kValue;
import static com.myanatol.app.constants.Constants.k_ID;

/**
 * ProfuileFragment to for user profile, attached to {@link HomeActivity}
 */
public class EventsFragment extends Fragment {
    public static final String TAG = EventsFragment.class.getSimpleName();

    private TextView tvLocation;
    private TextView tvStartTime;
    private TextView tvEndTime;
    private EditText edtEventName;
    private TYPE mType;
    private Event eventCalendar;
    private Button btnSave;
    private Button btnGonow;


    public EventsFragment() {
        // Required empty public constructor
    }

    public static EventsFragment newInstance(Event eventDay, TYPE type) {
        EventsFragment fragment = new EventsFragment();
        Bundle args = new Bundle();
        args.putSerializable(kType, type);
        args.putSerializable(kData, eventDay);
        fragment.setArguments(args);
        return fragment;
    }

    public static EventsFragment newInstance(TYPE type) {
        EventsFragment fragment = new EventsFragment();
        Bundle args = new Bundle();
        args.putSerializable(kType, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mType = (TYPE) getArguments().getSerializable(kType);
            if (Objects.requireNonNull(mType).equals(TYPE.EDIT))
                eventCalendar = (Event) getArguments().getSerializable(kData);
        }
        setHasOptionsMenu(mType.equals(TYPE.EDIT));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_events, container, false);
        initViews(view);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mType.equals(TYPE.EDIT))
            updateUI(eventCalendar);
    }

    private void updateUI(Event eventCalendar) {
        String eventName = eventCalendar.getTitle();
        String startTime = eventCalendar.getStartDatetime().getText();
        String endTime = eventCalendar.getEndDatetime().getText();
        edtEventName.setText(eventName);
        tvStartTime.setText(startTime);
        tvEndTime.setText(endTime);
        btnSave.setText(getString(R.string.update));

    }

    private void initViews(View view) {
        btnSave = view.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(v -> onSave());
        btnGonow = view.findViewById(R.id.btnGoNow);
        btnGonow.setOnClickListener(v -> goNow());
        tvLocation = view.findViewById(R.id.tvLocation);
        tvStartTime = view.findViewById(R.id.edtStart);
        tvEndTime = view.findViewById(R.id.edtEnd);
        edtEventName = view.findViewById(R.id.edtEventName);
        tvLocation.setOnClickListener(v -> onPlace());
        tvStartTime.setOnClickListener(v -> onStartTime());
        tvEndTime.setOnClickListener(v -> onEndTime());
    }

    private void onEndTime() {
        datePicker(tvEndTime);
    }

    private void onStartTime() {
        datePicker(tvStartTime);
    }

    private void datePicker(TextView textView) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(Objects.requireNonNull(getContext()),
                (view, year, monthOfYear, dayOfMonth) -> {
                    String date = Utils.checkDigit(dayOfMonth) + "-" + Utils.checkDigit(monthOfYear + 1) + "-" + Utils.checkDigit(year);
                    //*************Call Time Picker Here ********************
                    timePicker(date, textView);
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    private void timePicker(String date, TextView textView) {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                (view, hourOfDay, minute) -> {
                    String hourformat = Utils.get12HourFormatTime(hourOfDay, minute);
                    String time = date + " " + hourformat;
                    textView.setText(time);
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    private void onPlace() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .build(Objects.requireNonNull(getActivity()));
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void goNow() {
        ((HomeActivity) Objects.requireNonNull(getActivity())).goNowForEvent(true);
    }

    private void onSave() {
        String eventName = Utils.getProperText(edtEventName);
        String location = Utils.getProperText(tvLocation);
        String startTime = Utils.getProperText(tvStartTime);
        String endTime = Utils.getProperText(tvEndTime);
        Date startTimeDate = Utils.getTimeDate(startTime, Objects.requireNonNull(getContext()));
        Date endTimeDate = Utils.getTimeDate(endTime, getContext());

        if (eventName.isEmpty()) {
            edtEventName.setError("Event Title can't be empty");
            edtEventName.requestFocus();
            return;
        }
        if (mType.equals(TYPE.EDIT)) {
            HashMap<String, Object> parameter = new HashMap<>();
            HashMap<String, Object> parameterMap = getAllUpdatedParameters(eventName, location, startTimeDate, endTimeDate);

            parameter.put("data", getAllUpdatedParameters(eventName, location, startTimeDate, endTimeDate));
            ModelManager.modelManager().updateEvents(parameter, eventCalendar.getId(), (iStatus, response) -> {
                Toaster.toast(iStatus.name() + ": Event Successfully Saved!");
                Objects.requireNonNull(getActivity()).onBackPressed();
            }, (iStatus, error) -> {
                Toaster.toast(iStatus + "  " + error);
            });
        } else if (mType.equals(TYPE.CREATE)) {
            HashMap<String, Object> parameterMap = getAllParameters(eventName, location, startTimeDate, endTimeDate);
            ModelManager.modelManager().addEvents(parameterMap, (iStatus, response) -> {
                Toaster.toast(iStatus.name() + ": Event Successfully Saved!");
                ((HomeActivity) Objects.requireNonNull(getActivity())).onBackPressed();
            }, (iStatus, error) -> {
                Toaster.toast(iStatus + "  " + error);
            });
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        //updates toobar title
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.title_events));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(Objects.requireNonNull(getContext()), data);
                Log.i(TAG, "Place: " + place.getName());
                String placeText = (String) place.getName();
                tvLocation.setText(placeText);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(Objects.requireNonNull(getContext()), data);
                Log.i(TAG, status.getStatusMessage());
                Toaster.e(status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
                Toaster.e("No results");
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    public HashMap<String, Object> getEndTimeMap(Date endTime) {
        HashMap<String, Object> endTimeMap = new HashMap<>();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm a");
        String endTimeText = dateFormat.format(endTime);
        endTimeMap.put(kValue, String.valueOf(endTime.getTime() / 1000));
        endTimeMap.put(kText, endTimeText);
        endTimeMap.put(kTimezone, Utils.getCurrentTimeZone().getID());
        return endTimeMap;
    }

    public HashMap<String, Object> getUpdateEndTimeMap(Date endTime) {
        HashMap<String, Object> endTimeMap = new HashMap<>();
        endTimeMap.put(kValue, eventCalendar.getEndDatetime().getValue());
        endTimeMap.put(kText, eventCalendar.getEndDatetime().getText());
        endTimeMap.put(kTimezone, eventCalendar.getEndDatetime().getTimeZone());
        return endTimeMap;
    }

    @SuppressLint("SimpleDateFormat")
    public HashMap<String, Object> getStartTimeMap(Date startTime) {
        HashMap<String, Object> startTimeMap = new HashMap<>();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm a");
        String startTimeText = dateFormat.format(startTime);
        startTimeMap.put(kValue, String.valueOf(startTime.getTime() / 1000));
        startTimeMap.put(kText, startTimeText);
        startTimeMap.put(kTimezone, Utils.getCurrentTimeZone().getID());
        return startTimeMap;
    }

    public HashMap<String, Object> getUpdateStartTimeMap(Date startTime) {
        HashMap<String, Object> startTimeMap = new HashMap<>();
        startTimeMap.put(kValue, eventCalendar.getStartDatetime().getValue());
        startTimeMap.put(kText, eventCalendar.getStartDatetime().getText());
        startTimeMap.put(kTimezone, eventCalendar.getStartDatetime().getTimeZone());
        return startTimeMap;
    }

    public HashMap<String, Object> getAllParameters(String eventName, String location, Date startTime, Date endTime) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(kEndDateTime, getEndTimeMap(endTime));
        hashMap.put(k_ID, ModelManager.modelManager().getCurrentUser().getId());
        hashMap.put(kTitle, eventName);
        hashMap.put(kCommonEventId, ModelManager.modelManager().getCurrentUser().getCommonEventId());
        hashMap.put(kStartDateTime, getStartTimeMap(startTime));
        return hashMap;
    }

    public HashMap<String, Object> getAllUpdatedParameters(String eventName, String location, Date startTime, Date endTime) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(kEndDateTime, getUpdateEndTimeMap(endTime));
        hashMap.put(kTitle, eventName);
        hashMap.put(kCommonEventId, ModelManager.modelManager().getCurrentUser().getCommonEventId());
        hashMap.put(kStartDateTime, getUpdateStartTimeMap(startTime));
        hashMap.put(k_ID, eventCalendar.getId());
        return hashMap;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.event_menu, menu);
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_delete:
                onDelete();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onDelete() {
        String eventID = (eventCalendar!=null)?eventCalendar.getId():"";
        ModelManager.modelManager().deleteEvent(eventID, (iStatus) -> {
            Toaster.toast(iStatus.name() + ": Event Successfully Deleted!");
            Objects.requireNonNull(getActivity()).onBackPressed();
        }, (iStatus, error) -> {
            Toaster.toast(iStatus + "  " + error);
        });
    }

    public enum TYPE {
        CREATE,
        EDIT
    }
}
