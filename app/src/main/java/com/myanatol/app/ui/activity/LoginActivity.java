package com.myanatol.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.facebook.login.LoginManager;
import com.github.charbgr.authmanager.AuthManager;
import com.github.charbgr.authmanager.models.SuccessPayload;
import com.github.charbgr.authmanager.views.GoogleView;
import com.github.charbgr.authmanager.views.HintView;
import com.github.charbgr.authmanager.views.SmartLockView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialPickerConfig;
import com.google.android.gms.auth.api.credentials.CredentialRequest;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.myanatol.app.R;
import com.myanatol.app.constants.Constants;
import com.myanatol.app.data.network.model.SocialUser;
import com.myanatol.app.helper.FragmentUtil;
import com.myanatol.app.helper.Toaster;
import com.myanatol.app.helper.Utils;
import com.myanatol.app.managers.FacebookManager;
import com.myanatol.app.managers.ModelManager.ModelManager;
import com.myanatol.app.managers.ReachabilityManager;
import com.myanatol.app.ui.fragments.NewLoginFragment;
import com.myanatol.app.ui.fragments.SuccessFragment;

import java.util.Collections;
import java.util.HashMap;

import static com.myanatol.app.constants.Constants.kAccessToken;
import static com.myanatol.app.constants.Constants.kEmail;
import static com.myanatol.app.constants.Constants.kFacebookPublicProfile;
import static com.myanatol.app.constants.Constants.kFrom;
import static com.myanatol.app.constants.Constants.kPassword;
import static com.myanatol.app.constants.Constants.kProvider;
import static com.myanatol.app.constants.Constants.kTagAccounts;

/**
 * Base activity for Login module , all 2 {@link NewLoginFragment} {@link SuccessFragment}
 * are attached to it to perform base actions and switching between screens
 */
public class LoginActivity extends BaseActivity implements
        SuccessFragment.SuccessFragmentInteraction,
        NewLoginFragment.OnLoginInteractionListener,
        GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks,
        GoogleView, SmartLockView, HintView {

    private static final String TAG = LoginActivity.class.getSimpleName();
    FacebookManager.FacebookManagerInterface facebookManagerListener = new FacebookManager.FacebookManagerInterface() {
        @Override
        public void success(SocialUser socialUser) {
            signinFacebookUser(socialUser);
        }

        @Override
        public void failure(String s) {
            showProgress(false);
            Utils.showAlertDialog(LoginActivity.this, "Error",
                    "Error Completing Facebook authentication. Please try again, or if the issue persists, sign in using your email and password");
        }
    };
    private Constants.LOGIN_FROM mFrom;
    private String accessToken;
    private FacebookManager facebookManager;
    private AuthManager authManager;

    public static Intent getIntent(Context context, Constants.LOGIN_FROM from, String accessToken) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra(kFrom, from);
        intent.putExtra(kAccessToken, accessToken);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Intent intent = getIntent();
        if (intent != null) {
            mFrom = (Constants.LOGIN_FROM) intent.getSerializableExtra(kFrom);
            accessToken = intent.getStringExtra(kAccessToken);
        }

        facebookManager = new FacebookManager(this, facebookManagerListener);
        LoginManager.getInstance().registerCallback(facebookManager.getCallbackManager(),
                facebookManager.getFacebookCallback());
        if (mFrom.equals(Constants.LOGIN_FROM.login)) {
            FragmentUtil.changeFragment(getSupportFragmentManager(), NewLoginFragment.newInstance(), false, true);
        } else if (mFrom.equals(Constants.LOGIN_FROM.registration)) {
            FragmentUtil.changeFragment(getSupportFragmentManager(), NewLoginFragment.newInstance(), false, true);
        } else if (mFrom.equals(Constants.LOGIN_FROM.verification)) {
            FragmentUtil.changeFragment(getSupportFragmentManager(), SuccessFragment.newInstance("", accessToken), false, true);
        }
        initAuthManager();

    }

    public void requestCredential() {
        if (authManager != null)
            authManager.requestCredential();
    }

    private void initAuthManager() {
        GoogleApiClient googleApiClient = createGoogleApiClient();
        HintRequest hintRequest = createHintRequest();
        CredentialRequest smartLockRequest = createSmartlockCredentialsRequest();

        authManager = AuthManager
                .Builder(this)
                .withGoogleApiClient(googleApiClient)
                .withGoogle(this)
                .withHints(this, hintRequest)
                .withSmartLock(this, smartLockRequest)
                .build();

    }

    private CredentialRequest createSmartlockCredentialsRequest() {
        return new CredentialRequest.Builder()
                .setPasswordLoginSupported(true)
                .build();
    }

    private GoogleApiClient createGoogleApiClient() {
        return new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, createGoogleSignInOptions())
                .addApi(Auth.CREDENTIALS_API)
                .build();
    }

    private GoogleSignInOptions createGoogleSignInOptions() {
        return new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
    }

    private HintRequest createHintRequest() {
        return new HintRequest.Builder()
                .setHintPickerConfig(
                        new CredentialPickerConfig.Builder()
                                .setShowCancelButton(true)
                                .setPrompt(CredentialPickerConfig.Prompt.SIGN_IN)
                                .build()
                )
                .setEmailAddressIdentifierSupported(true)
                .build();
    }

    /**
     * {@link NewLoginFragment callback}
     *
     * @param email
     */
    @Override
    public void onLogin(String email) {
        HashMap<String, Object> parameterMap = new HashMap<>();
        parameterMap.put(kEmail, email);
        parameterMap.put(kProvider, "local");
        showProgress(true);
        ModelManager.modelManager().signupUser(parameterMap, (iStatus, response) -> {
            showProgress(false);
            FragmentUtil.changeFragment(getSupportFragmentManager(), SuccessFragment.newInstance(email, ""), true, true);
        }, (iStatus, error) -> {
            showProgress(false);
            Toaster.toast(iStatus + "  " + error);
        });
    }

    /**
     * {@link NewLoginFragment callback}
     * method for facebook login
     */
    @Override
    public void onFacebook() {
        if (!ReachabilityManager.getNetworkStatus()) {
            Utils.showAlertDialog(this, getString(R.string.login_failed),
                    getString(R.string.error_facebook_login));
            return;
        }
        showProgress(true);
        LoginManager.getInstance()
                .logInWithReadPermissions(this, Collections.singletonList(kFacebookPublicProfile));
    }

    private void loginUser(String email, String password) {
        showProgress(true);
        HashMap<String, Object> parameter = new HashMap<>();
        parameter.put(kEmail, email);
        parameter.put(kPassword, password);
        ModelManager.modelManager().loginUser(parameter, (iStatus) -> {
            showProgress(false);
            startActivity(HomeActivity.getIntent(LoginActivity.this));
            finish();
        }, (iStatus, error) -> {
            showProgress(false);
            Toaster.toast(iStatus + "  " + error);
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 64206) {
            facebookManager.getCallbackManager().onActivityResult(requestCode, resultCode, data);
        } else {
            authManager.handle(requestCode, resultCode, data);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        authManager.destroy();
    }

    private void signinFacebookUser(SocialUser socialUser) {
        ModelManager.modelManager().facebookSignupUser(socialUser.getJsonObject(), (iStatus) -> {
            showProgress(false);
            startActivity(HomeActivity.getIntent(LoginActivity.this));
            finish();
        }, (iStatus, error) -> {
            showProgress(false);
            Toaster.toast(iStatus + "  " + error);
        });
    }

    @Override
    public void googleAccessRevoked(Status status) {
        Log.v(kTagAccounts, "googleAccessRevoked");
    }

    @Override
    public void googleSignInResultFailure() {
        Log.v(kTagAccounts, "googleSignInResultFailure");
    }

    @Override
    public void googleSignOut(Status status) {
        Log.v(kTagAccounts, "googleSignOut");
    }

    @Override
    public void userCancelledGoogleSignIn() {
        Log.v(kTagAccounts, "userCancelledGoogleSignIn");
    }

    @Override
    public void emailHintRequestCancelled() {
        Log.v(kTagAccounts, "emailHintRequestCancelled");
    }

    @Override
    public void emailHintRequestFailure() {
        Log.v(kTagAccounts, "emailHintRequestFailure");
    }

    @Override
    public void emailHintSelected(Credential credential) {
        String email = credential.getId();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(NewLoginFragment.class.getName());
        if (fragment != null)
            ((NewLoginFragment) fragment).setEmail(email);
        Log.v(kTagAccounts, String.format("emailHintSelected %s %s ", credential.getId(), credential.getPassword()));
    }

    @Override
    public void credentialDelete(Status status) {
        Log.v(kTagAccounts, "credentialDelete");
    }

    @Override
    public void credentialRequestCancelled() {
        Log.v(kTagAccounts, "credentialRequestCancelled");
        authManager.requestEmailHints();
    }

    @Override
    public void credentialRequestFailure() {
        Log.v(kTagAccounts, "credentialRequestFailure");
        authManager.requestEmailHints();
    }

    @Override
    public void credentialRequestResolutionFailure() {
        Log.v(kTagAccounts, "credentialRequestResolutionFailure");
    }

    @Override
    public void credentialSaveFailure() {
        Log.v(kTagAccounts, "credentialSaveFailure");
    }

    @Override
    public void credentialSaveResolutionCancelled() {
        Log.v(kTagAccounts, "credentialSaveResolutionCancelled");
    }

    @Override
    public void credentialSaveResolutionFailure() {
        Log.v(kTagAccounts, "credentialSaveResolutionFailure");
    }

    @Override
    public void credentialSaveSuccess() {
        Log.v(kTagAccounts, "credentialSaveSuccess");
    }

    @Override
    public void signInSuccess(SuccessPayload successPayload) {
        if (successPayload.hasCredential()) {
            showCredentialDialog(successPayload.getCredential());
        } else if (successPayload.hasGoogleSignInAccount()) {
            String email = successPayload.getCredential().getId();
            String password = successPayload.getCredential().getPassword();
            loginUser(email, password);
        }
        Log.v(kTagAccounts, String.format("SignInSuccess %s %s ", successPayload.getCredential().getId(), successPayload.getCredential().getPassword()));
    }

    private void showCredentialDialog(Credential credential) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this).setTitle(R.string.credential_received)
                .setMessage(getString(R.string.what_do_you_want_to_do_with_credential, credential.getId()))
                .setPositiveButton(getString(R.string.use_credential), (dialog, which) -> {
                    String email = credential.getId();
                    String password = credential.getPassword();
                    loginUser(email, password);
                })
                .setNegativeButton(getString(R.string.delete_credential), (dialog, which) -> {
                    authManager.deleteCredential(credential);
                    dialog.dismiss();
                    dialog.cancel();
                });
        AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.v(kTagAccounts, "onConnected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v(kTagAccounts, "onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.v(kTagAccounts, "onConnectionFailed");
    }

    @Override
    public void onSuccess(String email, String password) {
        Credential credentialToSave = new Credential
                .Builder(email)
                .setPassword(password)
                .build();

        authManager.saveCredential(credentialToSave);
    }
}
