package com.myanatol.app.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myanatol.app.R;
import com.myanatol.app.data.network.model.Event;

import java.text.SimpleDateFormat;
import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    private Context context;
    private EventDayInteraction mListener;
    private List<Event> eventDayList;
    private SimpleDateFormat simpleDateFormat;

    @SuppressLint("SimpleDateFormat")
    public EventAdapter(Context context, List<Event> mlist, EventDayInteraction eventDayInteraction) {
        this.context = context;
        this.eventDayList = mlist;
        this.mListener = eventDayInteraction;
        this.simpleDateFormat = new SimpleDateFormat("HH:mm aaa");
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_event_day_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Event eventDay = eventDayList.get(position);
        holder.bindContent(eventDay);
        String name = eventDay.getTitle();
        String startDate = eventDay.getStartDatetime().getText();
        String endDate = eventDay.getEndDatetime().getText();
        holder.tvEventName.setText(name);
        holder.tvStartTime.setText(startDate);
        holder.tvEndTime.setText(endDate);


    }

    @Override
    public int getItemCount() {
        return eventDayList.size();
    }


    public interface EventDayInteraction {
        void onDaySelection(Event eventDay);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvStartTime;
        private TextView tvEndTime;
        private TextView tvEventName;
        private TextView tvEventLocation;
        private Event eventDay;

        ViewHolder(View itemView) {
            super(itemView);
            tvStartTime = itemView.findViewById(R.id.tvStartTime);
            tvEndTime = itemView.findViewById(R.id.tvEndTime);
            tvEventName = itemView.findViewById(R.id.tvEventName);
            tvEventLocation = itemView.findViewById(R.id.tvEventLocation);
            itemView.setOnClickListener(v -> {
                if (mListener != null)
                    mListener.onDaySelection(eventDay);
            });
        }

        void bindContent(Event metro) {
            this.eventDay = metro;
        }
    }
}
