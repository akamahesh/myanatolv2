package com.myanatol.app.ui.DialogFragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.myanatol.app.R;
import com.myanatol.app.data.network.model.Metro;

import java.io.Serializable;

import static com.myanatol.app.constants.Constants.kData;

/**
 * Bottom sheet dialog Fragment
 */
public class MetroMapBottomSheetDialog extends BottomSheetDialogFragment {

    private WebView webView;
    private TextView tvTitle;
    private Metro metro;
    private BottomSheetBehavior mBehavior;


    public static DialogFragment getInstance(Metro metro) {
        MetroMapBottomSheetDialog metroMapBottomSheetDialog = new MetroMapBottomSheetDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable(kData, metro);
        metroMapBottomSheetDialog.setArguments(bundle);
        return metroMapBottomSheetDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            metro = (Metro) getArguments().getSerializable(kData);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_bottom_sheet_metro_map, container, false);
        webView = view.findViewById(R.id.webView);
        tvTitle = view.findViewById(R.id.tvTitle);
        mBehavior = BottomSheetBehavior.from((View) view.getParent());
        view.findViewById(R.id.ivBack).setOnClickListener(v -> dismiss());
        return view;
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvTitle.setText(metro.getName());
        String fileName = metro.getPath();
        webView.loadUrl("file:///android_asset/MetroMap/" + fileName);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setInitialScale(0);
        webView.getSettings().setUseWideViewPort(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }
}
