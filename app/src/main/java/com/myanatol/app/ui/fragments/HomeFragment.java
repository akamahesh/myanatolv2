package com.myanatol.app.ui.fragments;

import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.myanatol.app.R;
import com.myanatol.app.data.network.model.CurrentUser;
import com.myanatol.app.data.network.model.Event;
import com.myanatol.app.helper.DividerItemRecyclerDecoration;
import com.myanatol.app.helper.Toaster;
import com.myanatol.app.helper.Utils;
import com.myanatol.app.managers.ModelManager.ModelManager;
import com.myanatol.app.ui.activity.BaseActivity;
import com.myanatol.app.ui.activity.HomeActivity;
import com.myanatol.app.ui.adapter.EventAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;

import static android.support.design.widget.BottomSheetBehavior.STATE_EXPANDED;
import static com.myanatol.app.constants.Constants.kDateOnly;
import static com.myanatol.app.constants.Constants.kLanguage;
import static com.myanatol.app.constants.Constants.kText;
import static com.myanatol.app.constants.Constants.kTimeZone;
import static com.myanatol.app.constants.Constants.kValue;

/**
 * Home Fragment, The very first screen of the home module, shows map and events .
 */
public class HomeFragment extends BaseFragment {

    public BottomSheetBehavior mBottomSheetBehavior;
    private RecyclerView rvEvents;
    private CoordinatorLayout coordinatorLayout;
    private TextView tvDateTitle;
    private TextView tvEmptyText;
    private List<Event> mEventList;
    private List<Event> filteredEventList;
    private DateFormat dateFormat;
    private EventAdapter eventAdapter;
    private ProgressBar progressBar;
    private HorizontalCalendar horizontalCalendar;


    public HomeFragment() {
        // Required empty public constructor

    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        dateFormat = new SimpleDateFormat("HH:mm a");

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initViews(view);
        filteredEventList = new ArrayList<>();
        eventAdapter = new EventAdapter(getContext(), filteredEventList, onEventDayInteraction);
        rvEvents.setAdapter(eventAdapter);


        /* starts before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        Calendar today = Calendar.getInstance();

        /* ends after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 2);
        horizontalCalendar = new HorizontalCalendar.Builder(view, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(7)
                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                //do something
                int month = date.get(Calendar.MONTH);
                String title = Utils.getMonthName(month) + " " + date.get(Calendar.YEAR);
                tvDateTitle.setText(title);
                loadEvents(date, mEventList);

            }
        });

        // The View with the BottomSheetBehavior
        View bottomSheet = coordinatorLayout.findViewById(R.id.bottomSheetEvent);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setPeekHeight(((BaseActivity) Objects.requireNonNull(getActivity())).getPeekHeight());
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change\
                if (newState == STATE_EXPANDED) {
                    bottomSheet.setOnTouchListener(null);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        FloatingActionButton floatingActionButton = view.findViewById(R.id.floatingActionButtonAdd);
        floatingActionButton.setOnClickListener(v -> {
            ((HomeActivity) Objects.requireNonNull(getActivity())).addEvent(null);
        });
        checkEmptyState(eventAdapter);
        return view;
    }


    private void checkEmptyState(EventAdapter eventAdapter) {
        if (eventAdapter.getItemCount() > 0) {
            tvEmptyText.setVisibility(View.INVISIBLE);
        } else {
            tvEmptyText.setVisibility(View.VISIBLE);
        }
    }

    EventAdapter.EventDayInteraction onEventDayInteraction = this::onEventSelection;

    private void onEventSelection(Event eventDay) {
        ((HomeActivity) Objects.requireNonNull(getActivity())).addEvent(eventDay);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        }
        // TODO: Move this to where you establish a user session
        logUser();

    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
        Crashlytics.setUserIdentifier(" " +currentUser.getId());
        Crashlytics.setUserEmail(" " +currentUser.getEmail());
        Crashlytics.setUserName(" " +currentUser.getDisplayName());

    }


    private void loadEvents(Calendar selectedDate, List<Event> mEventList) {
        showProgressScreen(true);
        Date date = selectedDate.getTime();
        String time = dateFormat.format(date);
        String timeStamp = String.valueOf(selectedDate.getTimeInMillis() / 1000);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(kDateOnly, "true");
        hashMap.put(kLanguage, "en");
        hashMap.put(kValue, timeStamp);
        hashMap.put(kText, time);
        hashMap.put(kTimeZone, Utils.getCurrentTimeZone().getID());
        ModelManager.modelManager().getEvents(hashMap, selectedDate, mEventList, (iStatus, response) -> {
            CopyOnWriteArrayList<Event> events = response.getObject();
            filteredEventList.clear();
            filteredEventList.addAll(events);
            eventAdapter.notifyDataSetChanged();
            showProgressScreen(false);
            checkEmptyState(eventAdapter);
        }, (iStatus, error) -> {
            Toaster.toast(error);
            showProgressScreen(false);

        });
    }

    void showProgressScreen(boolean progress) {
        if (progress) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkPermission() {
        if (Objects.requireNonNull(getActivity()).checkSelfPermission(android.Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED && getActivity().checkSelfPermission(android.Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.READ_CALENDAR, android.Manifest.permission.WRITE_CALENDAR}, 101);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
            if (mEventList == null)
                mEventList = Utils.getCalenderEvents(getContext());
            horizontalCalendar.goToday(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 101) {
            if (mEventList == null)
                mEventList = Utils.getCalenderEvents(getContext());
            horizontalCalendar.goToday(true);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        //updates toolbar title
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.title_home));

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //menuInflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }


    private void initViews(View view) {
        coordinatorLayout = view.findViewById(R.id.coordinator);
        progressBar = view.findViewById(R.id.progress_bar);
        tvDateTitle = view.findViewById(R.id.tvDate);
        tvEmptyText = view.findViewById(R.id.tvEmptyText);
        rvEvents = view.findViewById(R.id.rvEvents);
        rvEvents.setLayoutManager(new LinearLayoutManager(getContext()));
        rvEvents.addItemDecoration(new DividerItemRecyclerDecoration(Objects.requireNonNull(getContext())));
        rvEvents.setHasFixedSize(true);

    }
}
