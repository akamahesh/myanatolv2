package com.myanatol.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.myanatol.app.R;
import com.myanatol.app.data.network.model.Metro;
import com.myanatol.app.helper.Utils;

import java.io.Serializable;
import java.util.Objects;

import static com.myanatol.app.constants.Constants.kData;

/**
 * {@link MetroMapFragment} to show Maps contains a webview get redirects from {@link MetroFragment}
 */
public class MetroMapFragment extends Fragment {

    private WebView webView;
    private Metro mMetro;

    public MetroMapFragment() {
        // Required empty public constructor
    }


    /**
     * Getter method for MetroMapFragment instance
     * @param metro {@link Metro} object
     * @return new MetroMapFragment instance
     */
    public static MetroMapFragment newInstance(Metro metro) {
        MetroMapFragment fragment = new MetroMapFragment();
        Bundle args = new Bundle();
        args.putSerializable(kData, metro);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        if (getArguments() != null) {
            mMetro = (Metro) getArguments().getSerializable(kData);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_metro_map, container, false);
        initViews(view);
        Utils.hideKeyboard(getContext());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String fileName = mMetro.getPath();
        webView.loadUrl("file:///android_asset/MetroMap/" + fileName);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
    }


    @Override
    public void onStart() {
        super.onStart();
        //updates toolbar title
        Objects.requireNonNull(getActivity()).setTitle(mMetro.getName());
    }


    private void initViews(View view) {
        webView = view.findViewById(R.id.webView);
    }

}
