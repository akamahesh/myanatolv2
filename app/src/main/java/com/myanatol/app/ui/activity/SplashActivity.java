package com.myanatol.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import com.myanatol.app.R;
import com.myanatol.app.constants.Constants;
import com.myanatol.app.data.network.model.CurrentUser;
import com.myanatol.app.managers.BaseManager.BaseManager;
import com.myanatol.app.managers.ModelManager.ModelManager;

import java.util.Objects;

import static com.myanatol.app.constants.Constants.kCurrentUser;
import static com.myanatol.app.constants.Constants.kEmptyString;
import static com.myanatol.app.constants.Constants.kIsAlreadyOpened;

/**
 * Splash Activity decides where the app will be navigated, intro screens home screens or login screen .
 */
public class SplashActivity extends AppCompatActivity {
    private static final String TAG = SplashActivity.class.getSimpleName();
    private String accessToken;

    public static Intent getIntent(Context context){
        return new Intent(context,SplashActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        accessToken = getAccessToken();
        Log.v(TAG, accessToken);
        new Handler().postDelayed(() -> {
            CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
            String isAlreadyOpened = BaseManager.getDataFromPreferences(kIsAlreadyOpened, String.class);
            isAlreadyOpened = (isAlreadyOpened==null)?kEmptyString:isAlreadyOpened;
            if (currentUser != null) {
                startActivity(HomeActivity.getIntent(SplashActivity.this));
            } else if (TextUtils.isEmpty(accessToken)) {
                if (isAlreadyOpened.equalsIgnoreCase("yes"))
                    startActivity(LoginActivity.getIntent(SplashActivity.this, Constants.LOGIN_FROM.login, ""));
                else
                    startActivity(IntroScreenActivity.getIntent(SplashActivity.this));

            } else {
                startActivity(LoginActivity.getIntent(SplashActivity.this, Constants.LOGIN_FROM.verification, accessToken));

            }
            finish();

        }, 2000);


    }

    // ATTENTION: This was auto-generated to handle app links.
    public String getAccessToken() {
        String accessToken = "";
        try {
            Intent appLinkIntent = getIntent();
            Uri appLinkData = appLinkIntent.getData();
            String link = Objects.requireNonNull(appLinkData).toString();
            accessToken = link.substring(link.lastIndexOf('/') + 1, link.length());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return accessToken;
    }
}
