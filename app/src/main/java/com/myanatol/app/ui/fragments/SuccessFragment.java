package com.myanatol.app.ui.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.login.widget.LoginButton;
import com.myanatol.app.R;
import com.myanatol.app.constants.Constants;
import com.myanatol.app.data.network.model.BaseModel;
import com.myanatol.app.data.network.model.CurrentUser;
import com.myanatol.app.helper.FragmentUtil;
import com.myanatol.app.helper.Toaster;
import com.myanatol.app.helper.Utils;
import com.myanatol.app.helper.Validations;
import com.myanatol.app.managers.BaseManager.BaseManager;
import com.myanatol.app.managers.ModelManager.ModelManager;
import com.myanatol.app.ui.activity.HomeActivity;
import com.myanatol.app.ui.activity.LoginActivity;

import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;

import static com.myanatol.app.constants.Constants.kAccessToken;
import static com.myanatol.app.constants.Constants.kCurrentUser;
import static com.myanatol.app.constants.Constants.kEmail;
import static com.myanatol.app.constants.Constants.kPassword;

/**
 * SuccessFragment for login screen Stages of login module attached to {@link com.myanatol.app.ui.activity.LoginActivity}
 */
public class SuccessFragment extends BaseFragment {

    private static final String TAG = SuccessFragment.class.getSimpleName();
    private String email;
    private String accessToken;
    private Button btnContinue;
    private TextView tvTitle;
    private TextView tvSubTitle;
    private ImageView ivLogo;
    private String password;
    private SuccessFragmentInteraction mListener;


    public SuccessFragment() {
        // Required empty public constructor
    }


    public static SuccessFragment newInstance(String email,String accessToken) {
        SuccessFragment fragment = new SuccessFragment();
        Bundle args = new Bundle();
        args.putString(kEmail,email);
        args.putString(kAccessToken,accessToken);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SuccessFragment.SuccessFragmentInteraction) {
            mListener = (SuccessFragmentInteraction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement SuccessFragmentInteraction");
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            email = getArguments().getString(kEmail);
            accessToken = getArguments().getString(kAccessToken);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_success, container, false);


        tvTitle = view.findViewById(R.id.tvSubText);
        tvSubTitle = view.findViewById(R.id.tvDescription);
        ivLogo = view.findViewById(R.id.ivLogo);
        btnContinue = view.findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(onContinueListener);

        if(TextUtils.isEmpty(email)){
            //verification accesstoken
            finishRegistration(accessToken);

        }else{
            String title = getString(R.string.almost_done);
            String subText = "Please click on the link that has just been sent to your email %s and continue the registration process.";
            Drawable imageDrawable = getResources().getDrawable(R.drawable.ic_email_96dp,null);
            ivLogo.setImageDrawable(imageDrawable);
            tvTitle.setText(title);
            tvSubTitle.setText(String.format(subText,email));
            btnContinue.setVisibility(View.GONE);
        }
        Utils.hideKeyboard(getContext());
        return view;
    }

    Button.OnClickListener onContinueListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(btnContinue.getText().equals(getString(R.string.continue_text))){
                //success
                loginUser(email,password);
            }else{
                //failure
                Objects.requireNonNull(getActivity()).finishAffinity();
            }
        }
    };

    private void loginUser(String email, String password) {
        showProgress(true);
        HashMap<String,Object> parameter  = new HashMap<>();
        parameter.put(kEmail,email);
        parameter.put(kPassword,password);
        ModelManager.modelManager().loginUser(parameter,(iStatus)-> {
            showProgress(false);
            if(mListener!=null)
                mListener.onSuccess(email,password);
//            ((LoginActivity) Objects.requireNonNull(getActivity())).addCustomAccount(email,password);
            Objects.requireNonNull(getContext()).startActivity(HomeActivity.getIntent(getContext()));
            Objects.requireNonNull(getActivity()).finish();
        },(iStatus, error) -> {
            showProgress(false);
            Toaster.toast(iStatus+"  "+error);
            updateUI(iStatus);
        });
    }

    private void finishRegistration(String accessToken) {
        showProgress(true);
        ModelManager.modelManager().finishRegistration(accessToken,(iStatus, response)-> {
            HashMap<String,String> genericMap = response.getObject();
            email = genericMap.get(kEmail);
            password = genericMap.get(kPassword);
            showProgress(false);
            updateUI(iStatus);

        },(iStatus, error) -> {
            showProgress(false);
            Toaster.toast(iStatus+"  "+error);
            updateUI(iStatus);
        });
    }


    private void updateUI(Constants.Status type) {
        if(type.equals(Constants.Status.success)){
            String title = getString(R.string.congratulations);
            Drawable imageDrawable = getResources().getDrawable(R.drawable.ic_check_circle_96dp,null);
            ivLogo.setImageDrawable(imageDrawable);
            tvTitle.setText(title);
            tvSubTitle.setText(getString(R.string.your_account_has_been_successfully_setup));
            btnContinue.setText(getString(R.string.continue_text));
            btnContinue.setVisibility(View.VISIBLE);
        }else{
            String title = getString(R.string.oops);
            String subText = getString(R.string.something_went_wrong);
            Drawable imageDrawable = getResources().getDrawable(R.drawable.ic_error_96dp,null);
            ivLogo.setImageDrawable(imageDrawable);
            tvTitle.setText(title);
            tvSubTitle.setText(subText);
            btnContinue.setText(getString(R.string.exit));
            btnContinue.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }



    public interface SuccessFragmentInteraction{
        void onSuccess(String email, String password);
    }


}
