package com.myanatol.app.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.myanatol.app.R;
import com.myanatol.app.helper.Toaster;

import java.util.Objects;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.myanatol.app.constants.Constants.PLACE_AUTOCOMPLETE_REQUEST_CODE;

/**
 * {@link RouteHomeFragment} sample skeleton fragment for the sake of copy and integrity of the code.
 */
public class RouteHomeFragment extends Fragment {
    private final String TAG = RouteHomeFragment.class.getSimpleName();

    private TextView tvLocation;
    private TextView tvTiming;

    public RouteHomeFragment() {
        // Required empty public constructor
    }


    public static RouteHomeFragment newInstance() {
        RouteHomeFragment fragment = new RouteHomeFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_route_home, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_location:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onStart() {
        super.onStart();
        //updates toolbar title
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.title_route));

    }


    private void initViews(View view) {
        view.findViewById(R.id.floatingActionButtonhome).setOnClickListener(v -> onHome());
        view.findViewById(R.id.floatingActionButtonBusiness).setOnClickListener(v -> onBusiness());
        view.findViewById(R.id.floatingActionButtonAgenda).setOnClickListener(v -> onAgenda());
        view.findViewById(R.id.floatingActionButtonStar).setOnClickListener(v -> onStar());

        tvLocation = view.findViewById(R.id.tvLocation);
        tvTiming = view.findViewById(R.id.tvTiming);
        tvLocation.setOnClickListener(v -> onLocation());

    }

    private void onLocation() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .build(Objects.requireNonNull(getActivity()));
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(Objects.requireNonNull(getContext()), data);
                Log.i(TAG, "Place: " + place.getName());
                String placeText = (String) place.getName();
                tvLocation.setText(placeText);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(Objects.requireNonNull(getContext()), data);
                Log.i(TAG, status.getStatusMessage());
                Toaster.e(status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
                Toaster.e("No results");
            }
        }
    }

    private void onBusiness() {
    }

    private void onStar() {
    }

    private void onAgenda() {
    }

    private void onHome() {
    }

}
