package com.myanatol.app.ui.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.myanatol.app.helper.Utils;

/**
 * Base Fragment for common methods which can be used thoughout different fragments
 */
public class BaseFragment extends Fragment {
    private ProgressDialog progressDialog;

    public BaseFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Utils.generateProgressDialog(getContext(), false);
    }

    public void showProgress(boolean show) {
        if (show) {
            progressDialog.show();
        } else {
            if (progressDialog != null) progressDialog.dismiss();
        }
    }
}
