package com.myanatol.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.myanatol.app.R;
import com.myanatol.app.data.network.model.Event;
import com.myanatol.app.data.network.model.Metro;
import com.myanatol.app.helper.FragmentUtil;
import com.myanatol.app.helper.Toaster;
import com.myanatol.app.helper.Utils;
import com.myanatol.app.ui.fragments.EventsFragment;
import com.myanatol.app.ui.fragments.HomeFragment;
import com.myanatol.app.ui.fragments.MetroFragment;
import com.myanatol.app.ui.fragments.MetroMapFragment;
import com.myanatol.app.ui.fragments.ProfileFragment;
import com.myanatol.app.ui.fragments.RouteHomeFragment;
import com.myanatol.app.ui.fragments.StatisticsFragment;
import com.myanatol.app.ui.fragments.WebViewFragment;

import java.util.Objects;

public class HomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawer;
    private ActionBar mActionBar;
    private boolean mToolBarNavigationListenerIsRegistered = false;
    private boolean doubleBackToExitPressedOnce = false;

    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mActionBar = getSupportActionBar();
        mDrawer = findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                //method get ca
                super.onDrawerOpened(drawerView);
                drawerView.scrollTo(0, 0);
            }
        };
        mDrawer.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        FragmentUtil.changeFragment(getSupportFragmentManager(), HomeFragment.newInstance(), false, true);
        goForAgenda();

    }

    void goForAgenda() {
        Fragment homeFragment = getSupportFragmentManager().findFragmentByTag(HomeFragment.class.getName());
        if (homeFragment == null)
            FragmentUtil.changeFragment(getSupportFragmentManager(), HomeFragment.newInstance(), false, true);
        else
            FragmentUtil.changeFragment(getSupportFragmentManager(), homeFragment, false, true);
    }


    public void showMetroMap(Metro metro) {
        FragmentUtil.changeFragment(getSupportFragmentManager(), MetroMapFragment.newInstance(metro), true, true);
        showUpButton(true);
    }


    @Override
    public void onBackPressed() {
        try {
            Utils.hideKeyboard(this);
            if (mDrawer.isDrawerOpen(GravityCompat.START)) {
                mDrawer.closeDrawer(GravityCompat.START);
                mDrawer.scrollTo(0, 0);

            } else {
                int backStackCount = getSupportFragmentManager().getBackStackEntryCount();

                if (backStackCount >= 1) {
                    getSupportFragmentManager().popBackStack();
                    // Change to hamburger icon if at bottom of stack
                    if (backStackCount == 1) {
                        showUpButton(false);

                        //open drawer when backstack entry is 1 that is home screen
                        //on QA reqest opening drawer
                        //mDrawer.openDrawer(GravityCompat.START);
                        //mDrawer.scrollTo(0,0);
                    }


                } else {
                    if (doubleBackToExitPressedOnce) {
                        super.onBackPressed();
                        return;
                    }
                    this.doubleBackToExitPressedOnce = true;
                    Toaster.toast(getString(R.string.validation_press_again));
                    new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addEvent(Event eventDay) {
        if (eventDay == null)
            FragmentUtil.changeFragment(getSupportFragmentManager(), EventsFragment.newInstance(EventsFragment.TYPE.CREATE), true, true);
        else
            FragmentUtil.changeFragment(getSupportFragmentManager(), EventsFragment.newInstance(eventDay, EventsFragment.TYPE.EDIT), true, true);
        showUpButton(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            //home/up logic handled by onbackpressed
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_agenda) {
            // Handle the camera action
            goForAgenda();
        } else if (id == R.id.nav_navigation) {
            goNowForEvent(false);
        } else if (id == R.id.nav_metro) {
            FragmentUtil.changeFragment(getSupportFragmentManager(), MetroFragment.newInstance(), true, true);
            showUpButton(true);
        } else if (id == R.id.nav_statistics) {
            FragmentUtil.changeFragment(getSupportFragmentManager(), StatisticsFragment.newInstance(), true, true);
            showUpButton(true);
        } else if (id == R.id.nav_profile) {
            FragmentUtil.changeFragment(getSupportFragmentManager(), ProfileFragment.newInstance(), true, true);
            showUpButton(true);
        } else if (id == R.id.nav_rate) {
            Utils.rateApplication(this);
        } else if (id == R.id.nav_confidentiality) {
            FragmentUtil.changeFragment(getSupportFragmentManager(), WebViewFragment.newInstance("confidential.html", getString(R.string.title_confidentiality)), true, true);
            showUpButton(true);
        } else if (id == R.id.nav_general_condition) {
            FragmentUtil.changeFragment(getSupportFragmentManager(), WebViewFragment.newInstance("conditions.html", getString(R.string.title_Terms_conditions)), true, true);
            showUpButton(true);
        } else if (id == R.id.nav_legal) {
            FragmentUtil.changeFragment(getSupportFragmentManager(), WebViewFragment.newInstance("legal.html", getString(R.string.title_legal)), true, true);
            showUpButton(true);
        } else if (id == R.id.nav_feedback) {
            Utils.feedbackApplication(this);
        } else if (id == R.id.nav_share) {
            Utils.shareApplication(this);
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    protected void onStart() {
        super.onStart();
        Objects.requireNonNull(this).setTitle(getString(R.string.title_home));
    }

    private void resolveUpButtonWithFragmentStack() {
        showUpButton(getSupportFragmentManager().getBackStackEntryCount() > 0);
    }


    private void showUpButton(boolean show) {
        // To keep states of ActionBar and ActionBarDrawerToggle synchronized,
        // when you enable on one, you disable on the other.
        // And as you may notice, the order for this operation is disable first, then enable - VERY VERY IMPORTANT.
        if (show) {
            // Remove hamburger
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            // Show back button
            mActionBar.setDisplayHomeAsUpEnabled(true);
            // when DrawerToggle is disabled i.e. setDrawerIndicatorEnabled(false), navigation icon
            // clicks are disabled i.e. the UP button will not work.
            // We need to add a listener, as in below, so DrawerToggle will forward
            // click events to this listener.
            if (!mToolBarNavigationListenerIsRegistered) {
                mDrawerToggle.setToolbarNavigationClickListener(v -> onBackPressed());

                mToolBarNavigationListenerIsRegistered = true;
            }

        } else {
            // Remove back button
            mActionBar.setDisplayHomeAsUpEnabled(false);
            // Show hamburger
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            // Remove the/any drawer toggle listener
            mDrawerToggle.setToolbarNavigationClickListener(null);
            mToolBarNavigationListenerIsRegistered = false;
        }

    }

    public void goNowForEvent(boolean showUpButton) {
        Fragment routeHomeFragment = getSupportFragmentManager().findFragmentByTag(RouteHomeFragment.class.getName());
        if (routeHomeFragment == null)
            FragmentUtil.changeFragment(getSupportFragmentManager(), RouteHomeFragment.newInstance(), true, true);
        else
            FragmentUtil.changeFragment(getSupportFragmentManager(), routeHomeFragment, true, true);

        showUpButton(showUpButton);
    }


}

