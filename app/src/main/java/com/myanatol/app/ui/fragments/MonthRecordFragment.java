package com.myanatol.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.myanatol.app.R;
import com.myanatol.app.ui.activity.HomeActivity;
import com.myanatol.app.ui.adapter.MonthlyAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * MonthRecordFragment for statistics screen
 */
public class MonthRecordFragment extends Fragment {
    private RecyclerView recyclerView;
    private List<String> mList;
    private MonthlyAdapter monthlyAdapter;


    public MonthRecordFragment() {
        // Required empty public constructor
    }


    public static MonthRecordFragment newInstance() {
        MonthRecordFragment fragment = new MonthRecordFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        if (getArguments() != null) {

        }
        mList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_month_record, container, false);
        initViews(view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mList.add("one ");
        mList.add("one ");
        mList.add("one ");
        mList.add("one ");
        monthlyAdapter.notifyDataSetChanged();

    }


    private void initViews(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        monthlyAdapter = new MonthlyAdapter(getContext(), mList);
        recyclerView.setAdapter(monthlyAdapter);
    }


}
