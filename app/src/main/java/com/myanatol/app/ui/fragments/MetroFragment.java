package com.myanatol.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.myanatol.app.R;
import com.myanatol.app.data.DataManager;
import com.myanatol.app.data.network.model.Metro;
import com.myanatol.app.helper.DividerItemRecyclerDecoration;
import com.myanatol.app.ui.activity.HomeActivity;
import com.myanatol.app.ui.adapter.MetroAdapter;

import java.util.List;
import java.util.Objects;

/**
 * MetroFragment to hold all metro list, attached to {@link HomeActivity}
 */
public class MetroFragment extends Fragment {

    private RecyclerView recyclerView;
    private MetroAdapter metroAdapter;
    private List<Metro> metroList;

    private SearchView.OnQueryTextListener onQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            //filter recycler view when query submit
            metroAdapter.getFilter().filter(query);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            metroAdapter.getFilter().filter(newText);
            return false;
        }
    };


    public MetroFragment() {
        // Required empty public constructor
    }

    public static MetroFragment newInstance() {
        MetroFragment fragment = new MetroFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        if (getArguments() != null) {

        }
        metroList = DataManager.getDataManager().getMetroList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_metro, container, false);
        initViews(view);
        metroAdapter = new MetroAdapter(getContext(), metroList, metroInteraction);
        recyclerView.setAdapter(metroAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(metroAdapter);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemRecyclerDecoration(getContext(), R.drawable.canvas_recycler_divider);
        recyclerView.addItemDecoration(itemDecoration);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void initViews(View view) {
        SearchView searchView = view.findViewById(R.id.searchView);
        recyclerView = view.findViewById(R.id.recyclerViewMetro);
        searchView.setOnQueryTextListener(onQueryTextListener);
    }

    MetroAdapter.MetroInteraction metroInteraction = metro -> {
        ((HomeActivity) Objects.requireNonNull(getActivity())).showMetroMap(metro);
    };

    @Override
    public void onStart() {
        super.onStart();
        //updates toobar title
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.title_metros));
    }

}
