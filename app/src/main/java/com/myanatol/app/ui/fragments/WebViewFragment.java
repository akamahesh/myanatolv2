package com.myanatol.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.myanatol.app.R;

import java.util.Objects;

import static com.myanatol.app.constants.Constants.kData;
import static com.myanatol.app.constants.Constants.kTitle;

/**
 * {@link WebViewFragment } fragment to be used for loading html content only!
 */
public class WebViewFragment extends Fragment {

    private WebView webView;
    private String mFileName;
    private String title;

    public WebViewFragment() {
        // Required empty public constructor
    }


    /**
     * Getter method for fragment instance
     * @param fileName html file name that to be shown
     * @param title    title for the toolbar of the page
     * @return new WebViewFragment instance
     */
    public static WebViewFragment newInstance(String fileName, String title) {
        WebViewFragment fragment = new WebViewFragment();
        Bundle args = new Bundle();
        args.putString(kData, fileName);
        args.putString(kTitle, title);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        if (getArguments() != null) {
            mFileName = getArguments().getString(kData);
            title = getArguments().getString(kTitle);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_web_view, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        // to update toolbar title every time fragment switches
        Objects.requireNonNull(getActivity()).setTitle(title);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        webView.loadUrl("file:///android_asset/TermsPages/" + mFileName);
    }


    private void initViews(View view) {
        webView = view.findViewById(R.id.webView);
    }

}
