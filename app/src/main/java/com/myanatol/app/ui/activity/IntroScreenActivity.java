package com.myanatol.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myanatol.app.R;
import com.myanatol.app.constants.Constants;
import com.myanatol.app.managers.BaseManager.BaseManager;

import static com.myanatol.app.constants.Constants.kIsAlreadyOpened;

public class IntroScreenActivity extends AppCompatActivity {
    private int[] layouts;
    private Button btnGetStarted;
    private LinearLayout dotLayout;



    public static Intent getIntent(Context context) {
        return new Intent(context, IntroScreenActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_screen);

        ViewPager viewPager = findViewById(R.id.viewPager);
        dotLayout = findViewById(R.id.viewDots);
        btnGetStarted = findViewById(R.id.btnGetStarted);
        btnGetStarted.setOnClickListener(v -> onLogin());
        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = new int[]{
                R.layout.layout_slide_a,
                R.layout.layout_slide_b,
                R.layout.layout_slide_c,
                R.layout.layout_slide_d,
                R.layout.layout_slide_e,
                R.layout.layout_slide_f
        };

        //adding bottom dots
        addBottomDots(0);


        SectionPagerAdapter sectionPagerAdapter = new SectionPagerAdapter();
        viewPager.setAdapter(sectionPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    void onLogin() {
        BaseManager.saveDataIntoPreferences("yes", kIsAlreadyOpened);
        startActivity(LoginActivity.getIntent(this, Constants.LOGIN_FROM.login, ""));
        finish();
    }

    void onRegister() {
        BaseManager.saveDataIntoPreferences("yes", kIsAlreadyOpened);
        startActivity(LoginActivity.getIntent(this, Constants.LOGIN_FROM.registration, ""));
        finish();
    }

    private void addBottomDots(int currentPage) {
        TextView[] dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }


    class SectionPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        SectionPagerAdapter() {

        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert layoutInflater != null;
            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

}
