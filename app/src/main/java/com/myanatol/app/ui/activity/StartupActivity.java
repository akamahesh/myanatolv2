package com.myanatol.app.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Launcher Screen To single entry point to the app
 */
public class StartupActivity extends AppCompatActivity {
    private static final String TAG = StartupActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(SplashActivity.getIntent(this));
        finish();
    }
}
