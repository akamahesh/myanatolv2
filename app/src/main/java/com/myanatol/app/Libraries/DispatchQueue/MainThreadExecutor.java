package com.myanatol.app.Libraries.DispatchQueue;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.Executor;

/**
 * Created by akaMahesh on 19/11/16.
 * Copyright to Mobulous Technology Pvt. Ltd.
 */

class MainThreadExecutor implements Executor {
    private final Handler handler = new Handler(Looper.getMainLooper());

    @Override
    public synchronized void execute(Runnable runnable) {
        handler.post(runnable);
    }

}
