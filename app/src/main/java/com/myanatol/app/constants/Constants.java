package com.myanatol.app.constants;

import java.util.Objects;

/**
 * To hold all the constants keys and enums throughout the app.
 */
public interface Constants {

    //TAGS
    String kTagAccounts = "AnatolAccountLogs ==> ";
    String kTagAPI = "AnatolAPILogs ==> ";
    String kTagTracking = "AnatolTracking ==> ";


    //common constants used
    String kEmptyString = "";
    Integer kEmptyNumber = 0;
    String kFrom = "from";
    String kType = "type";
    String kIsAlreadyOpened = "opened";
    String kAccessToken = "access_token";
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 101;

    //api key constants
    String kEmail = "email";
    String kProvider = "provider";
    String kPassword = "password";
    String kDateOnly = "dateOnly";
    String kLanguage = "language";
    String kValue = "value";
    String kText = "text";
    String kTimeZone = "timeZone";
    String kData = "data";
    String kEndDateTime = "endDatetime";
    String kTimezone = "time_zone";
    String k_ID = "_id";
    String kTitle = "title";
    String kCommonEventId = "commonEventId";
    String kStartDateTime = "startDatetime";

    //current user keys
    String kId = "_id";
    String kDisplayName = "displayName";
    String kFamilyName = "familyName";
    String kGivenName = "givenName";
    String kMiddleName = "middleName";
    String kPhone = "phone";
    String kHomePlaceId = "homePlaceId";
    String kBusinessPlaceId = "businessPlaceId";
    String kFavoritePlaceId = "favouritePlaceId";
    String kIsActivated = "isActivated";
    String kIsSuperUser = "isSuperuser";
    String kName = "name";
    String kPhotos = "photos";
    String kRoles = "roles";
    String kV = "__v";
    String kUpdateAt = "updatedAt";
    String kCreatedAt = "createdAt";
    String kUser = "user";

    //Event keys




    String kDefaultAppName = "MyAnatol";
    String kAppPreferences = "MyAnatolAppPreferences";

    String kStatus = "Status";
    String kMessage = "message";
    String kCurrentUser = "currentUser";

    String kMessageServerNotRespondingError = kDefaultAppName + " server not responding!";
    String kMessageInternalInconsistency = "Some internal inconsistency occurred. Please try again.";
    String kMessageNetworkError = "Device does not connect to internet.";
    String kSocketTimeOut = kDefaultAppName + " Server not responding..";

    //facebook constants

    String kID = "id";
    String kFacebookFirstName = "first_name";
    String kFacebookLastName = "last_name";
    String kFacebookGender = "gender";
    String kFacebookFields = "fields";
    String kFacebookAllFields = "id,name,link,email,picture,first_name,last_name,gender,friends";
    String kFacebookEmail = "email";
    String kFacebookPublicProfile = "public_profile";


    /**
     * enum to define From where the LoginActivity was initiated.
     */
    enum LOGIN_FROM {
        login,
        registration,
        verification,
        autheticator
    }


    /**
     * Status Enumeration for Task Status
     */
    enum Status {
        success(0),
        fail(1),
        reachLimit(2),
        noChange(3),
        history(4),
        normal(5),
        discard(6);

        private int value;

        Status(int status) {
            this.value = status;
        }

        public static Status getStatus(int value) {
            for (Status status : Status.values()) {
                if (status.value == value) {
                    return status;
                }
            }
            return fail;
        }

        /**
         * To get Integer value of corresponding enum
         */
        public Integer getValue() {
            return this.value;
        }
    }


    /**
     * Http Status for Api Response
     */
    enum HTTPStatus {
        success("SUCCESS"),
        failure("FAILURE"),
        error("ERROR");

        private String httpStatus;

        HTTPStatus(String httpStatus) {
            this.httpStatus = httpStatus;
        }

        public static HTTPStatus getStatus(String status) {
            for (HTTPStatus httpStatus : HTTPStatus.values()) {
                if (Objects.equals(httpStatus.httpStatus, status)) {
                    return httpStatus;
                }
            }
            return error;
        }

        public String getValue() {
            return this.httpStatus;
        }


    }
}


