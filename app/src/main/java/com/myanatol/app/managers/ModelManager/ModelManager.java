package com.myanatol.app.managers.ModelManager;

import android.util.Log;

import com.google.gson.Gson;
import com.myanatol.app.Libraries.DispatchQueue.DispatchQueue;
import com.myanatol.app.constants.Blocks.Block;
import com.myanatol.app.constants.Blocks.GenricResponse;
import com.myanatol.app.constants.Constants;
import com.myanatol.app.data.network.model.BaseModel;
import com.myanatol.app.data.network.model.CurrentUser;
import com.myanatol.app.data.network.model.Event;
import com.myanatol.app.helper.Utils;
import com.myanatol.app.managers.APIManager.APIManager;
import com.myanatol.app.managers.APIManager.APIRequestHelper;
import com.myanatol.app.managers.BaseManager.BaseManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.myanatol.app.managers.APIManager.APIRequestHelper.kLoginUser;


/**
 * Created by akaMahesh on 15/3/18.
 */

/**
 * Singleton class to manage all models, data management and data processing in projects.
 */
public class ModelManager extends BaseManager implements Constants {

    private static final String TAG = ModelManager.class.getSimpleName();
    //Static properties
    private static ModelManager _ModelManger;
    private static String mGenericAuthToken = "";
    //Instance Properties
    private static CurrentUser mCurrentUser = null;
    private Gson gson;
    private DispatchQueue dispatchQueue =
            new DispatchQueue("com.queue.serial.modelmanager", DispatchQueue.QoS.userInitiated);


    /**
     * private constructor
     */
    private ModelManager() {
        this.gson = new Gson();
    }

    /**
     * method to create a threadsafe singleton class instance
     */
    public static synchronized ModelManager modelManager() {
        if (_ModelManger == null) {
            _ModelManger = new ModelManager();

            mCurrentUser = getDataFromPreferences(kCurrentUser, CurrentUser.class);
            Log.i(TAG, "Current User : " + ((mCurrentUser == null) ? null : mCurrentUser.toString()));
        }
        return _ModelManger;
    }

    /**
     * to initialize the singleton object
     */
    public void initializeModelManager() {
        System.out.print("ModelManager object initialized.");
    }

    /**
     * Stores {@link CurrentUser} to the share preferences and synchronize sharedpreferece
     */
    public synchronized void archiveCurrentUser() {
        saveDataIntoPreferences(mCurrentUser, BaseModel.kCurrentUser);
    }

    /**
     * getter method for genericAuthToken
     */
    public synchronized String getGenericAuthToken() {
        return mGenericAuthToken;
    }


    public DispatchQueue getDispatchQueue() {
        return dispatchQueue;
    }


    public synchronized CurrentUser getCurrentUser() {
        return mCurrentUser;
    }

    public synchronized void setCurrentUser(CurrentUser o) {
        mCurrentUser = o;
        archiveCurrentUser();
    }

    /**
     * method will be called to get all the Event list
     *
     * @param parameters request parameters
     * @param success    Block passed as callback for success condition
     * @param failure    Block passed as callback for failure condition
     */
    public void getEvents(HashMap<String, Object> parameters, Calendar date, List<Event> mEventList, Block.Success<CopyOnWriteArrayList<Event>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(String.format(APIRequestHelper.kGetEvents, getCurrentUser().getId()), parameters, (Status iStatus, GenricResponse<Object> genricResponse) -> {
                try {
                    Object o = genricResponse.getObject();
                    Gson gson = new Gson();
                    CopyOnWriteArrayList<Event> events = new CopyOnWriteArrayList<>();
                    JSONArray jsonArray = new JSONArray(gson.toJson(o));
                    String commonId = "";
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Event event = new Event(jsonObject);
                        commonId = event.getCommonEventId();
                        events.add(event);
                    }
                    mCurrentUser.setCommonEventId(commonId);
                    archiveCurrentUser();
                    for (Event event : mEventList) {
                        if (event.getStartDatetime().getCalender().get(Calendar.DATE) == date.get(Calendar.DATE) &&
                                (event.getStartDatetime().getCalender().get(Calendar.MONTH) == date.get(Calendar.MONTH))) {
                            events.add(event);
                        }
                    }
                    GenricResponse<CopyOnWriteArrayList<Event>> genericList =
                            new GenricResponse<>(events);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> DispatchQueue.main(() -> failure.iFailure(statusFail, message)));
        });
    }

    /**
     * create event method
     * @param parameters request parameters
     * @param success    Block passed as callback for success condition
     * @param failure    Block passed as callback for failure condition
     */
    public void addEvents(HashMap<String, Object> parameters, Block.Success<Object> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager().processFormRawRequest(String.format(APIRequestHelper.kCreateEvents, getCurrentUser().getId()), parameters, (Status iStatus, GenricResponse<Object> genricResponse) -> {
                try {
                    Object jsonObject = genricResponse.getObject();
                    GenricResponse<Object> genericList =
                            new GenricResponse<>(jsonObject);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> DispatchQueue.main(() -> failure.iFailure(statusFail, message)));
        });
    }
    /**
     * create event method
     * @param  eventId
     * @param status    Block passed as callback for success condition
     * @param failure    Block passed as callback for failure condition
     */
    public void deleteEvent(String eventId, Block.Status status, Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager().processDeleteRequest(String.format(APIRequestHelper.kDeleteEvents, getCurrentUser().getId(),eventId), (Status iStatus, GenricResponse<Object> genricResponse) -> {
                try {
                    Object jsonObject = genricResponse.getObject();
                    DispatchQueue.main(() -> status.iStatus(iStatus));
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> DispatchQueue.main(() -> failure.iFailure(statusFail, message)));
        });
    }


    public void updateEvents(HashMap<String, Object> parameters, String eventId, Block.Success<Object> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager().processFormPutRequest(String.format(APIRequestHelper.kUpdateEvents, getCurrentUser().getId(), eventId), parameters, (Status iStatus, GenricResponse<Object> genricResponse) -> {
                try {
                    Object jsonObject = genricResponse.getObject();
                    GenricResponse<Object> genericList =
                            new GenricResponse<>(jsonObject);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> DispatchQueue.main(() -> failure.iFailure(statusFail, message)));
        });
    }


    public void signupUser(HashMap<String, Object> parameters, Block.Success<Object> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(APIRequestHelper.kRegisterUser, parameters, (Status iStatus, GenricResponse<Object> genricResponse) -> {
                try {
                    Object jsonObject = genricResponse.getObject();
                    GenricResponse<Object> genericList =
                            new GenricResponse<>(jsonObject);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> DispatchQueue.main(() -> failure.iFailure(statusFail, message)));
        });
    }


    public void facebookSignupUser(JSONObject jsonParameterObject, Block.Status status, Block.Failure failure) {
        dispatchQueue.async(() -> {
            HashMap<String, Object> parameters = Utils.jsonToMap(jsonParameterObject);
            APIManager.APIManager().processRawRequest(APIRequestHelper.kFacebookSignin, parameters, (Status iStatus, GenricResponse<Object> genricResponse) -> {
                try {
                    Object o = genricResponse.getObject();
                    JSONObject jsonObject = new JSONObject(gson.toJson(o));
                    CurrentUser currentUser = new CurrentUser(jsonObject);
                    setCurrentUser(currentUser);

                    DispatchQueue.main(() -> status.iStatus(iStatus));
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> DispatchQueue.main(() -> failure.iFailure(statusFail, message)));
        });
    }

    public void finishRegistration(String accessToken, Block.Success<HashMap<String, String>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager().processGetRequest(accessToken, (Status iStatus, GenricResponse<Object> genricResponse) -> {
                try {
                    Object o = genricResponse.getObject();
                    JSONObject jsonObject = new JSONObject(gson.toJson(o));
                    String email = jsonObject.getString(kEmail);
                    String password = jsonObject.getString(kPassword);
                    HashMap<String, String> responseMap = new HashMap<>();
                    responseMap.put(kEmail, email);
                    responseMap.put(kPassword, password);

                    GenricResponse<HashMap<String, String>> genericList =
                            new GenricResponse<>(responseMap);
                    DispatchQueue.main(() -> success.iSuccess(iStatus, genericList));
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> DispatchQueue.main(() -> failure.iFailure(statusFail, message)));
        });
    }


    public void loginUser(HashMap<String, Object> parameters, Block.Status status, Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager().processRawRequest(kLoginUser, parameters, (Status iStatus, GenricResponse<Object> genricResponse) -> {
                try {
                    Object o = genricResponse.getObject();
                    JSONObject jsonObject = new JSONObject(gson.toJson(o));
                    CurrentUser currentUser = new CurrentUser(jsonObject);
                    setCurrentUser(currentUser);
                    DispatchQueue.main(() -> status.iStatus(iStatus));
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> DispatchQueue.main(() -> failure.iFailure(statusFail, message)));
        });
    }
}
