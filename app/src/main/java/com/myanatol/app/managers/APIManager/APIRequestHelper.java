package com.myanatol.app.managers.APIManager;

import com.google.gson.JsonObject;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by akaMahesh on 15/3/18.
 * Copyright to Mobulous Technology Pvt. Ltd.
 */

public interface APIRequestHelper {


    //static URL


    //api key for logout
    String kGetEvents = "users/%s/events/by-date/";
    String kCreateEvents = "users/%s/events/";
    String kUpdateEvents = "users/%s/events/%s";
    String kDeleteEvents = "users/%s/events/%s";
    String kRegisterUser = "authorization/register/";
    String kFacebookSignin = "authorization/sign-in-with-facebook";
    String kLoginUser = "authorization/login/";
    String kFinishRegistration = "authorization/finish-registration/";




    /**
     * set api request with api key and corresponding parameters
     *
     * @param APIKey key for the url
     * @param details details contains request body parameters
     * @param files if include file will be sent in multipart
     * @return JsonObject i.e. response.
     */
    @Multipart
    @POST()
    Call<Object> APIRequestWithFile(
            @Url String APIKey,
            @PartMap Map<String, RequestBody> details,
            @Part List<MultipartBody.Part> files
    );


    /**
     * set api request with api key with json raw request
     * @param APIKey
     * @param params
     * @return
     */
    @POST()
    Call<Object> APIRequestRaw(
            @Url String APIKey,
            @Body RequestBody params
    );

    @PUT()
    Call<Object> APIPutRequestRaw(
            @Url String APIKey,
            @Body RequestBody params
    );

    @DELETE()
    Call<Object> APIDeleteRequest(
            @Url String APIKey
    );

    @FormUrlEncoded
    @POST()
    Call<Object> APIFormUrlEncodedRequest(
            @Url String APIKey,
            @Field("data") String data
    );

    @GET("authorization/finish-registration/{accessToken}")
    Call<Object> APIGetRequest(@Path("accessToken") String apiKey);


}
